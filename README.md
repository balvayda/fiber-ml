# FIBER-LM 1.0

**FIBER**-LM is a graphical interface allowing to segment batches of histological images (tiff) from an initial learning phase. It provides segmented maps by tissue class and a pixel count of each class.

This is a public repository including both the executable version of FIBER-LM 1.0 and the open source code, in Matlab  which requires Matlab(r) 2019b or earlier and the Image Toolbox and the Statistical and ML Toolbox to run.

Fiber was designed for targeted staining only ( color carries information)

## steps of the analysis

1) Settings : Define/Select of the workspace of the analysis (it can work on multiple studies)
2) Learning : Define/select labels and associate pixels of the leaning images to such labels (or classes)
3) Localization (option) : Define one or several regions of interest in the images
4) Production : Learning data are used to fit the classifier wich is applied on pixels of all the images of the study
5) Visual Control : Control visually the segmentation on a sample of images and fill (option) a form according to your comments
6) Summary : Provide a tab with counting and quality checking of the all process


See the Manual in FIBER_ML_EXE of more details.

