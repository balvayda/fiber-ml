function Hist_Prod_tracerRois(mode,repData,repRoi)
% Draw Regions of interest (ROI)
% IN %
% mode : 'untreated','treated','all','manual'; type of data (or derived
% data) to process
% repData and repRoi (option): directories (names) of study data and Rois 
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% to do: rename Hist_Prod_tracerRoi -> Hist_Prod_drawRoi
% to do: include default names and colors

% if not provided by variables, fix the directory names by default
if ~exist('repData','var') || isempty(repData)
    repData=('../ProdData/');
    if ~isfolder(repData)
        mkdir(repData);
    end
end
if ~exist('repRoi','var') || isempty(repData)
    repRoi=('../ProdRoi/');
    if ~isfolder(repRoi)
        mkdir(repRoi);
    end
end

[~,fichData]=dirq(repData,'');
[~,fichRoi]=dirq(repRoi,'');

nomData=regexprep(fichData,'.tif','');                  % data files (tif images)
nomRoi=regexprep(fichRoi,{'roi_','.mat'},{'',''});      % Roi files (.mat)

modifRoi=0;     % we don't erase old ROIs if they exist

switch mode % data to process according to mode, data and ROI files
    case 'untreated'
        % Data without Roi
        [~,iData]=setxor(nomData,nomRoi);
        iData=iData';
    case 'treated'
        [~,iData,iRoi]=intersect(nomData,nomRoi);
         modifRoi=1;     % overwrite old Rois
    case 'all'
        % start all over again
        iData=(1:numel(nomData));
    case 'manual' % multiple selection
        [iData,ok]=listdlg('ListString',nomData);
        nomDatai=nomData(iData);
        for i=1:numel(nomDatai)
            [~,~,tmp]=intersect(nomDatai{i},nomRoi);
            if isempty(tmp)
                iRoi(i)=-1;
            else
                iRoi(i)=tmp;
            end
        end
        if ~isempty(iRoi)
               modifRoi=1;
        else
            modifRoi=0;
        end
       
end


for i=1:numel(iData)
    nomFichData=fichData{iData(i)};
    im=imread([repData,nomFichData]);
    im=im(:,:,1:3);
    [m,n,c]=size(im);
    hf=figure('units','normalize','position',[0 0.05 1 0.85]);hi=imagesc(im);
    nomD=regexprep(nomData{iData(i)},'_',' ');
    axis image;title(nomD,'Interpreter','latex');
    
    %%%%%%%%%%%%%%%%%%%%
    if modifRoi && iRoi(i)>0
        nomFichRoi=fichRoi{iRoi(i)};
        nomtot=[repRoi,nomFichRoi];
        if exist(nomtot,'file')
            kgpIni=load(nomtot);
            nom1Roi=regexprep(nomFichRoi,'.mat','');
            if isfield(kgpIni,'masque') % compatible with older version
                kgpIni.masc=kgpIni.masque;
                kgpIni=rmfield(kgpIni,'masque');
            end
            if isfield(kgpIni,'masc') % compatible with older version
                pgs=traceRoiS(hf,gca,hi,kgpIni,nom1Roi);
            else
                pgs=traceRoiS(hf,gca,hi,kgpIni.pgs,nom1Roi);  % verions in line with traceRoiS
            end
        else
            pgs=traceRoiS(hf,gca,hi,'',nom1Roi);
        end
    else
        pgs=traceRoiS(hf,gca,hi,'',nomRoi);
    end
    %%%%%%%%%%%%%%%%%%%%
    if ~isempty(pgs) || pgs.break==0   % 2 cd argument for compatibility v5b
        disp('sauvegarde')
        nomFichRoi=regexprep(nomFichData,'.tif','.mat');
        nomFichRoi=cat(2,'roi_',nomFichRoi),
        save([repRoi,nomFichRoi],'pgs');
        
    else 
         return;
    end


    disp(i);
    close(hf);
end
