function Hist_Appr_activScatter3D(pix,class,coul,h,ho)
% 
% displays the pixels classified in the color space (channels)
% during learning ("appr" in the programm name), replace by "learn" code in next version

% nota : to do, generalize from 3D to ND

% IN : 
%   pix : list of pixel values (RGB)
%   class : class/label of each pixel in pix
%   coul : list of colors per class
%   h : figure object (handle) where the scatter is drawn
%   ho : option, the figure to activate at the end (give hand back to the main window)

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% 1 / darken classes colors for better visibility vs background
coul2=rgb2hsv(coul);
coul2(:,2)=sqrt(coul2(:,2));
coul2(:,3)=(0.9*coul2(:,3)).^2;
coul=hsv2rgb(coul2);

% 2 / current figure
if ~exist('h','var') || isempty(h)
    h=figure;
end
set(0,'CurrentFigure',h);
ha=gca;
ha.Toolbar.Visible='on';

if ~isempty(pix)    % TODO : 3D only 
    scatter3(pix(:,1),pix(:,2),pix(:,3),20,coul(class,:),'fill');
    nombre = hist(class,unique(class));
    title(nombre(:)'); %#ok<HIST> % count pixels of each class
else
    scatter3([],[],[]);
    %nombre= [0 0 0];
end

box on;
xlabel('Red');ylabel('Green');zlabel('Blue');


if exist('ho','var')  
    set(0,'CurrentFigure',ho);
end