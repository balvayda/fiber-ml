function rep=defineRepEnv()
% define the folder which collect by defauld information on 
% various working environment
% for FIBER-LM1.0 D.Balvay PARCC U970 Inserm/Univ. Paris, France.

% write /ENV/ next to the current folder
[~,rep]=system('cd'); %% WARNING WINDOWS ONLY
rep=updir(rep);
rep=[rep,'\ENV\'];
