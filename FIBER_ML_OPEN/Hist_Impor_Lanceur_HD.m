function Hist_Impor_Lanceur()

% pour changer les r�peroires d'entr�e et de sortie, modifier les donn�es
% dans 'Init_import.mat' et sauver avec le m�me nom
% External function to include in FIBER-LM post1.0
s=load('Init_import.mat');
% ecriture des repertoires s'ils n'existent pas
if ~exist(s.rep.dataSource,'dir')
    mkdir(s.rep.dataSource);
end
if ~exist(s.rep.dataImporte,'dir')
    mkdir(s.rep.dataImporte);
end
if ~exist(s.rep.dataTrace,'dir')
    mkdir(s.rep.dataTrace);
end
disp('Valeurs par defaut')
disp('resolution image cadrage (nb pix / mm)')
resPreview=10,
disp('resolution image capturee');
resFinal=1000, % pixel par mm pour les captures;
disp('------');

% liste tous les fichiers du r�pertoire
[~,fichs]=dirq(s.rep.dataSource,'');
% demande les fichiers brutes � traiter en multis�lection
if isempty(fichs)
    fichs=uigetfile([s.rep.dataSource,fichs{1}],'MultiSelect','on');
else
    disp('ERREUR : aucun fichier dans le dossier source');
end


if ~iscell(fichs) % s'il n'y a qu'un fichier
    Hist_Impor_importerCadran(fichs,s.rep,resPreview,'resolution',resFinal);
else
    nf=numel(fichs);
    for i=1:nf
        if i==1 % lance l'interface d'importation
            [~,resFinal]=Hist_Impor_importerCadran(fichs{i},s.rep,resPreview,'resolution',resFinal);
        else
            Hist_Impor_importerCadran(fichs{i},s.rep,resPreview,'resolution',resFinal);
        end
    end
end
