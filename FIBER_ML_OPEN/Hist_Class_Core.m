function [classif,cls]=Hist_Class_Core(im,hroi,learnList,cls)
% Core of the classification : classify pixels in an image or
% a region of interest(ROI) in this image, given a learning list provided
% at the previous step (learning)
% In FIBER-LM1.0, the classification model is the discriminant aanlysis (DA)

% IN :
% im : image to be classified (segmented)
% hroi : a Roi object (structure)
% learnList : learning list 
% cls : option : classifier previously fitted according to listApp

% OUT :
% classif : structure of results corresponding to classification
% cls : fitted DA model (classifier and settings)
% MODIF :

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

nb_classes=numel(learnList.classeDef.nom);  % number of classes
nx=size(im,1); % image size
ny=size(im,2);


% fitting (optimisation) if required (lack of cls)
if ~exist('cls','var') || isempty(cls)   
    % no apriori concerning the number of pixels per class, so prior =
    % uniform
    cls=fitcdiscr(double(learnList.pixels), learnList.classes, 'DiscrimType', 'pseudoQuadratic', 'prior', 'uniform');
    % criteria on the model
    cvmodel = crossval(cls);
    cross = kfoldLoss(cvmodel);
    loose = loss(cls,double(learnList.pixels), learnList.classes);

end

% format pixels as a list
if exist('hroi','var') && ~isempty(hroi)
    
    mascc=cut(hroi.masc,hroi.pg);    % crop masc (copy)
    imLoc=cut(im,hroi.pg);         % crop image (copy)
    pixs=empile(imLoc,mascc);  % the list of pixels included in the masc
    one_masque=1;
else
    
    pixs=empile(im);        % list of pixels in the image
    one_masque=0;
end

%%%%%%%%%%classification of the pixel list(application of the model)
% label list, probability...
[label,probs,cost]=predict(cls,double(pixs)); % label = label list
count=hist(label,1:nb_classes);
%%%%%%%%%%%%%%%%%%%%%%%%
label=single(label);
probs=single(probs);
cost=single(probs);

if one_masque==0    
    mascc=ones(nx,ny);
    label=depile(label,mascc); % reformat classes in a map
    probs=depile(probs,mascc); % reformat des probabilities
    cost=depile(cost,mascc); % and costs, according to their position in the image
else
    label=depile(label,mascc,nan,'cut'); %  idem when there is a masc (class of the outside = 0)
    probs=depile(probs,mascc,nan,'cut');
    cost=depile(cost,mascc,nan,'cut');
end

% parametric maps, counts and metadata

    % infos on parametric maps, for display
    
classif.map.brut.im=label;
classif.map.brut.colmap=learnList.classeDef.couleur;
classif.map.brut.nom='classes';


classif.map.cost.im=cost;
classif.map.cost.colmap=jet; 
classif.map.cost.nom='cost';

classif.map.prob.im=probs;
classif.map.prob.colmap=hsv; 
classif.map.cost.nom='proba';


    % info on counting
classif.compt.nom=learnList.classeDef.nom;
classif.compt.brut=count;


classif.roi=hroi;
classif.listApp=learnList;
if exist('cross','var')
    classif.info.cross=cross;
    classif.info.loss=loose;
end
