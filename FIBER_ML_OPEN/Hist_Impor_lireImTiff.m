function [im,trace]=Hist_Impor_lireImTiff(nomfich,mode,valeur,rect)
% read a field in a level of a multi level tif type image
% external from Fiber, not final

% for FIBER-LM post 1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

%IN
% nomfich : complete file name
% mode : field to select the level in the stack of images in the file={'taille','resolution','numero'}
% valeur : value of the field defining the scale
% rect : rectangle of the selection for the field

disp(">> ouverture");
disp(nomfich);


% 1 /key information
warning off
info=imfinfo(nomfich);
warning on
W=[info.Width];
H=[info.Height];
T=max([W;H]);           % image sizes

R=[info.XResolution];   % resolution
U={info.ResolutionUnit}; % unit

nomU={'Centimeter','Inch'};
valU=[10 25.4];
for i=1:numel(R)    % conversion in mm
    j=strcmp(nomU,U{i});
    R(i)=R(i)/valU(j);
end



% 2/ Evaluation of the INDEX 

%[R,iR]=sort(R);   % sorted by increasing resolutions
%T=T(iR);
FOV=T./R;       % field of vue (FOV) in mm
[~,irmax]=max(R);
[~,ifov]=uniquetol(FOV,.1,'OutputAllIndices',true);  % indices of the various FOV
for i=1:numel(ifov)  
    if ~isempty(intersect(ifov{i},irmax))
        inds=ifov{i};  % indices compatibles with the FOV of the higher  resolution 
    end
end


%we want T or R lower or equal to the indicative value but in line with the column/row ration of the full scale. 
% If more than one we take the greater 
switch mode     % ordered index
    case 'numero'
        index=valeur;
    case 'taille'
        index=find(T<=valeur);
    case 'resolution'
        index=find(R<=valeur);
end
switch mode
    case {'taille','resolution'}
        % if an image with a sufficiently low scale in the stack
        if ~isempty(index)
            Rmax=max(R(index)); %we take the greatest of them if several
            index=find(R==Rmax,1,'First');
            index=intersect(index,inds);            % in line with full sized ?
        end
        if isempty(index)                   % si no we select the smaler image with a scale higher than expected
            Rmin=min(R(inds));         
            index=find(R==Rmin,1,'First');
        end
end

if isempty(index) || index>numel(R)
    error('ERR: lireImTiff : condition de selection impossible');
end

disp('index :');
disp(index);
nx=W(index);
ny=H(index);


% 3/ 



if ~exist('rect','var') || isempty(rect)
    im=imread(nomfich,'Index',index);           % no spatial information 
else
    xs=rect{1};ys=rect{2};
    xs=[max(1,ceil(xs(1))) min(floor(xs(2)),nx)];
    ys=[max(1,ceil(ys(1))) min(floor(ys(2)),ny)];
    
    try

        Obim=bigimage(nomfich);
        corn1=[x(1) y(1)];
        corn2=[x(2) y(2)];
        im=getRegion(Obim,index,corn1,corn2);
    catch
        disp('f');
        im=imread(nomfich,'Index',index,'PixelRegion',{ys,xs});
    end
end

trace.taille=T;                % ordered sizes
trace.resolution=R;            % ordered resolutions
trace.index=index;
trace.info=info;      % infos about the read level


if 1>0
    disp('Resolution Taille couche');
    info=info(index);
    W=[info.Width];
    H=[info.Height];
    T=max([W;H]);           % image sizes
    R=[info.XResolution];   % résolution
    U={info.ResolutionUnit}; % unit
    nomU={'Centimeter','Inch'};
    valU=[10 25.4];
    for i=1:numel(R)    % to mm
        j=strcmp(nomU,U{i});
        R(i)=R(i)/valU(j);
    end
    disp([ R, T, index])
end