function im=cut(im,hroi)
% crop an image from a roi object of from a derived rectangle
% FOR FIBER-LM 1.0 D. D.Balvay - 07/01/2022 - PARCC U970 Inserm/Univ. Paris, France

if isobject(hroi) % if hroi a not trivial rectangle
    if isprop(hroi,'Vertices')
        pos=hroi.Vertices;
    else
        pos=hroi.Position;
    end
    mini=ceil(min(pos,[],1));
    maxi=floor(max(pos,[],1));

else    % hypothesis
    cadre=hroi; % a rectangle
    mini=ceil(cadre(1,:));
    maxi=floor(cadre(2,:));
end
mini=max([mini;1 1]);
[nx,ny,~]=size(im);
maxi=min([maxi;ny nx]);

if ismatrix(im)% if 2D
    try
       im=im(mini(2):maxi(2),mini(1):maxi(1));
    catch
        disp('debug');
    end
else
    try
       im=im(mini(2):maxi(2),mini(1):maxi(1),:);
    catch
        disp('debug');
    end
    
end