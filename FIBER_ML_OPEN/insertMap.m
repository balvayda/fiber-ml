function im=insertMap(im,map,gamma,colmap,rectangle,lim)
% overlays a parametric map (map) on an original image (im)

% IN
% im : histological picture
% map : parametric map to overlay
% gamma : opacity
% colmap : colormap = color code
% nomNorm : type of normalisation for the map
% rectangle (option), [minx maxx;miny maxy] , map corners 
% lim (option), intensity thresholds
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.
ncol=size(colmap,1);
% init masc by nans
masc=~isnan(map);
masc=repmat(masc,[1 1 3]);

nd=ndims(map);  
if nd==2    % convert in RGB if necessary
    if ~exist('lim','var') || isempty(lim)
        mapRGB=ind2rgb(map,colmap);      
    else
        mini=lim(1);maxi=lim(2);  % if in float, the range is required
        map=1+(ncol-1)/(maxi-mini)*(map-mini); % normalisation
        map=max(map,1);                      % 
        map=min(map,ncol);
        map=round(map);
        mapRGB=ind2rgb2(map,colmap);
    end
end


formatIm=class(im);  % cast

switch formatIm
    case 'uint8'  % not perfect : to do
        mapRGB=im2uint8(mapRGB);
    case 'uint16'
        mapRGB=im2uint16(mapRGB);
    case 'double'
        mapRGB=im2double(mapRGB);
end


if exist('rectangle','var') && ~isempty(rectangle)
    mini=rectangle(1,:);
    maxi=rectangle(2,:);
    mini=max([mini;1 1]);
    [nx,ny,~]=size(im);
    maxi=min([maxi;ny nx]);
    
    imLoc=cut(im,rectangle);
    imLoc(masc)=(1-gamma)*imLoc(masc)+gamma*mapRGB(masc);
    im(mini(2):maxi(2),mini(1):maxi(1),:)=imLoc;
else
    im(masc)=(1-gamma)*im(masc)+gamma*mapRGB(masc);
end