function [pgs,pg,hi]=traceRoiS(hf,ha,hi,npg,nomImage,index0,paff0,nomRoi,purge) 
% pgs=traceRoi(hf,ha,hi,kpg)
% Create complex exclusive Rois (kgp) from an graphical image object (hi)
% included in a given axes object (ha) of a figure (hf). All created
% previously

% npg : initial rois, if any, includes : masc, paff and index.
% nomImage : name of the image file on which the Roi is drawn
% options
% index : initial roi number
% paff : display settings 
% nomRoi : expected names of the rois (of npg)
% purge : if ==1, firstly clear previous Rois in the figure

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% To Do : the attribution of colors and names should be clarified
% to do : add new ROI not so intuitive, to be improved
% 

%set(hf,'WindowStyle','Modal');

ha.XLimMode ='manual';
ha.YLimMode ='manual';
if exist('nomRoi','var') && ~isempty(nomRoi)
    setappdata(hf,'nomRoi',nomRoi)
end

pgs=findobj(hf,'type','polygon');  % find polygons in the figure


% 1/ Data to manage

% indices identification
if exist('index0','var') && ~isempty(index0)
   index=index0;
else
   index=1;
   index0=index;
end
setappdata(hf,'index0',index0); % initial index 
setappdata(hf,'index',index); % = current index
 
% display settings
if exist('paff0','var') && ~isempty(paff0)
    setappdata(hf,'paff0',paff0); % parametres d'affichage courants
end

% clean old Rois if asked
if exist('purge','var') && purge>0
    delete(pgs);
end

axes(ha);
hold on;

% include initial ROIs  (ngp) if provided. npg is disassembled to create a new object
if exist('npg','var') && ~isempty(npg)
    inpg=getindex(npg);
    nnpg=numel(npg);
    [~,ordre]=sort(inpg);
    npg=npg(ordre);
    for i=1:nnpg
        
        pg=trace(hf,ha,npg(i));  % draw the polygon number i
        pgs=fusion(hf,pg,pgs,'+'); % fusion with previous ones in the figure
    end
end

[nomIndex,numIndex]=createIndexName(hf);


% 2 / Action buttons

% include the toolbar
set(ha,'units','normalized');
pos=get(ha,'outerposition');
marge=.1;
if pos(2)<marge 
    pos2=pos(2);
    pos(2)=marge;
    pos(4)=pos(4)-marge+pos2;
    set(ha,'outerposition',pos);
end

hg=uibuttongroup(hf,'units','normalized','position',[0 0 1 .1]);
setappdata(hf,'hbar',hg);

% create buttons
hstop=uicontrol('parent',hg,'units','normalized','position',[.01 .1 .1 .8],'string',' Stop ');

hidxs=uicontrol('parent',hg,'units','normalized','position',[.15 .5 .1 .4],...
    'style','popupmenu','string',nomIndex,'Value',numIndex);
uicontrol('parent',hg,'units','normalized','position',[.15 .01 .1 .4],...
    'style','text','string','index ROI'); 
hidxPlus=uicontrol('parent',hg,'units','normalized','position',[.25 .1 .1 .8],'string',' Index +');


hplus=uicontrol('parent',hg,'units','normalized','position',[.5 .1 .1 .8],'string',' + ');
hmoins=uicontrol('parent',hg,'units','normalized','position',[.6 .1 .1 .8],'string',' - ');

hraz=uicontrol('parent',hg,'units','normalized','position',[.7 .1 .1 .8],'string',' reset ');
hall=uicontrol('parent',hg,'units','normalized','position',[.8 .1 .1 .8],'string',' all ');

hok=uicontrol('parent',hg,'units','normalized','position',[.9 .1 .1 .8],'string',' OK ');

% attribute a function per button
set(hstop,'callback',{@annuler,hg});

set(hidxs,'callback',{@indexInList,hf}); 
set(hidxPlus,'callback',{@autreIndex,hf});  

set(hplus,'callback',{@modifRoi,hf,ha,'+'});
set(hmoins,'callback',{@modifRoi,hf,ha,'-'});

set(hraz,'callback',{@razer,hf});
set(hall,'callback',{@total,hf,ha,hi});

set(hok,'callback',{@valider,hg});

setappdata(hf,'hidxs',hidxs); % indices

% manage action associated to the legend
hl=legend();
set(hl,'ItemHitFcn',{@changeCoul,hf});

cm = uicontextmenu;
uimenu(cm,'Text','No','MenuSelectedFcn',{@editFromLegend,hf,'No'});
uimenu(cm,'Text','Color','MenuSelectedFcn',{@editFromLegend,hf,'Color'});
uimenu(cm,'Text','Name','MenuSelectedFcn',{@editFromLegend,hf,'Name'});

hl.ContextMenu=cm;






% END
waitfor(hg,'Tag')


switch hg.Tag
    case 'fin'
        index=getappdata(hf,'index'); % selection of the current pg
        pgs=findobj(hf,'type','polygon');  
        idxs=getindex(pgs);
        pg=pgs(idxs==index);
        
        [~,is]=sort(idxs);  % save sorted polygons (based on their index)
        pgs=pgs(is);
        %names
        
        for i=1:numel(pgs)
            
            if exist('nomImage','var') && ~isempty(nomImage)
                pgs(i).UserData.nomImage=nomImage;
            end
        end

    case 'nul'
        pg=[];
        pgs=[];  
end
delete (hg); % remove the toolbar

end

%%%%%%FUNCTIONS  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function editFromLegend(~,~,hf,nom)
setappdata(hf,'editLegend',nom);
end

function changeCoul(~,event,hf) % change the color of the current ROI
nom=getappdata(hf,'editLegend');
if ~isempty(nom)
    switch nom
        case 'Color'
            c = uisetcolor(event.Peer.FaceColor);
            event.Peer.FaceColor=c;
        case 'Name'
            answer = inputdlg('ROI name','Select', [1 50]);
            nomR=answer{1};
            setnomRoi(event.Peer,nomR);
            actualiseListe(hf);
    end
    
end

end

function indexInList(ici,~,hf) % set the current index graphically / text

index=ici.Value;
npg=findobj(hf,'type','polygon'); 
inpg=getindex(npg);
inpg=sort(inpg);
index2=inpg(index);
disp(index2);
setappdata(hf,'index',index2);

end

function autreIndex(~,~,hf) % add a new index to the index list

pgs=findobj(hf,'type','polygon');  
ng=numel(pgs);

if ng>0
    idxs = getindex(pgs); % index list
else
    idxs=1;
end

prompt = {'Enter new index:'};
dlgtitle = 'Input';
dims = [1 35];% by default
answer = inputdlg(prompt,dlgtitle,dims,{num2str(max(idxs+1))});

index =str2double(answer{1});
if isnan(index)
    disp('Erreur : numerical value expected');
    return
end

if ~isempty(intersect(idxs,index))
    disp('existing Roi number');
    setappdata(hf,'index',index);
else
    setappdata(hf,'index',index);
    
   
    actualiseListe(hf)
end

end

function actualiseListe(hf)

[nomIndex,valeur]=createIndexName(hf);
hidxs=getappdata(hf,'hidxs');
set(hidxs,'string',nomIndex);
set(hidxs,'value',valeur);
    
end

function [noms,valeur]=createIndexName(hf) 
pgs=findobj(hf,'type','polygon');
nomRoi=getnomRoi(pgs);

ng=numel(pgs);

if ng>0
    idxs = getindex(pgs); 
else
    idxs=1;
end


index0=getappdata(hf,'index0');
index=getappdata(hf,'index');
if ~isempty(index0)
    indices=union(idxs,index0); %  Roi on a new index, from variables (initialisation)
end
if ~isempty(index)
    indices=union(indices,index); % Roi on a new index, from the corresponding button
end


ni=numel(indices);
noms=cell(ni,1);

for i=1:ni
    ind=find(idxs==indices(i));
    if isempty(ind) || isempty(nomRoi)
        noms{i}=['ROI ',num2str(indices(i))];
    else
        noms{i}=nomRoi{ind};
   end
    
end
valeur=find(indices==index);
end

function modifRoi(~,~,hf,ha,mode)


hbar=getappdata(hf,'hbar');
set(hbar,'Visible','off');
pgs=findobj(hf,'type','polygon');
pg=trace(hf,ha);  % draw manually

pgs=fusion(hf,pg,pgs,mode);
set(hbar,'Visible','on');
end

function valider(~,~,hg)
set(hg,'Tag','fin');

end

function annuler(~,~,hg)
set(hg,'Tag','nul');

end

function razer(~,~,hf)   % remove any Roi for the current index
index=getappdata(hf,'index');

pgs=findobj(hf,'type','polygon');  
idxs=getindex(pgs);

delete (pgs(idxs==index));

end

function total(~,~,hf,ha,hi)  % take all
pgs=findobj(hf,'type','polygon');  % polygons available in the figure
ipgs=getindex(pgs);
ipg=getappdata(hf,'index');

pg=pgs(ipgs==ipg);
if ~isempty(pg)
    paff=getpaff(pg);
end

delete(pgs);


hroi=getappdata(hf,'hroi');
num=getappdata(hf,'num');
if num>0
    hroi(num).Visible='off';
end
[nx,ny,~]=size(get(hi,'CData'));
masc=true(nx,ny);
warning off
ps=polyshape([0 0 ny ny],[0 nx nx 0 ],'Simplify',true);  % creation of a polygone
warning on
axes(ha);
pg=plot(ps);
setmasc(pg,masc);
if isequal(ipg,getappdata(hf,'index0'))
    setpaff(pg,getappdata(hf,'paff'));
elseif exist('paff','var')
    setpaff(pg,paff);
end

setindex(pg,ipg);
setmasc(pg,masc);
pg.DisplayName=['ROI ',num2str(getindex(pg))];

end


function pg=trace(hf,ha,pg)

% manage the drawing after fusion (existing ROi and new drawn one)
warning on
if ~exist('pg','var')
    hroi=drawfreehand(ha);   % draw a new ROI
    pos=hroi.Position;       % get positions
    pos=pos(1:end-1,:);
    warning off
    ps=polyshape(pos,'Simplify',true);  % create a polygone
    index=getappdata(hf,'index'),
    masc=createMask(hroi);   % create a masque
    delete(hroi);            % remove the polygone in the image
    nomRoi=['ROI ',num2str(index)];
    neo=1;
    
else
    if isfield(pg,'masc')  % manage an older version with a unique ROI
        ps=pg.pg;
        index=1;
        paff.FaceColor='r';
        masc=pg.masc;
        nomRoi='ROI 1';
        
        %vrs='5b'; % version 5b
    else
        ps=pg.Shape;  % get pieces of pg
        index=getindex(pg);
        paff=getpaff(pg);
        masc=getmasc(pg);
        nomRoi=getnomRoi(pg);
        
        if isempty(nomRoi)
            nomRoi=['ROI ',num2str(index)];
        end
        %vsr='5c';   % version 5c
    end
    neo=0;
end
axes(ha);

% draw ps and provide a associated graphical object pg
pg=plot(ps); % 

setindex(pg,index);
setmasc(pg,masc);
if ~isempty(nomRoi)
    setnomRoi(pg,nomRoi);
    actualiseListe(hf);
end

if neo==0  % paff exist yet
    setpaff(pg,paff);
end

end


% function index
function index = getindex(pg)

npg=numel(pg);
index=zeros(npg,1);
for i=1:npg
    try
        index(i)=pg(i).UserData.index;
    catch
        index(i)=1;  % for an older verion
    end
end

end

function setindex(pg,index)

pg.UserData.index=index;

end


function setnomRoi(pg,nomRoi)
if iscell(nomRoi)
    pg.DisplayName=nomRoi{1};
else
    pg.DisplayName=nomRoi;
end
end

function nomRoi = getnomRoi(pg)

npg=numel(pg);

    nomRoi=cell(npg,1);
    for i=1:npg
        if ~isempty(pg(i).DisplayName)
            nomRoi{i}=pg(i).DisplayName;
        else
            nomRoi{i}='';  % 
        end
    end

end




% function paff
function paff = getpaff(pg)

% graphical settings
nomChamp={'FaceAlpha','FaceColor','linestyle','lineWidth','EdgeAlpha','EdgeColor'};
nc=numel(nomChamp);
for i=1:nc
    nomC=nomChamp{i};
    paff.(nomC)=get(pg,nomC);
end

end

function setpaff(pg,paff)

nomChamp={'FaceAlpha','FaceColor','linestyle','lineWidth','EdgeAlpha','EdgeColor'};
nc=numel(nomChamp);
if ~isempty(paff)
    for i=1:nc
        nomC=nomChamp{i};
        if isfield(paff,nomC)
            set(pg,nomC,paff.(nomC));
        end
    end
end

end

% function masc
function masc = getmasc(pg)

if isstruct(pg.UserData) && isfield(pg.UserData,'masc')
    masc=pg.UserData.masc;
else
    masc=pg.UserData;   % for old version
    disp('masque mode compatibilité')
end

end


function setmasc(pg,masc)
pg.UserData.masc=masc;

end

    % fusion of the polyshape (in one index) with the previous ones
function pgs=fusion(hf,pg,pgs,mode)

ipg=getindex(pg);
ipgs=getindex(pgs);
npgs=numel(pgs);

[ipgs,ordre]=sort(ipgs);
pgs=pgs(ordre);

for i=1:npgs    %  index present yet on the figure (Ex)
    pgEx=pgs(i);
    ipgEx=ipgs(i);
    paff=getpaff(pgEx);
    if ipgEx==ipg  
        modi=mode;
        
    else
        modi='-';   % pg provides indications
    
    end
    switch modi
        case '+'
            psout=union (pgEx.Shape,pg.Shape);
            pgout=plot(psout);
            setmasc(pgout,getmasc(pgEx) | getmasc(pg)); % union 
        case '-'
            psout=subtract (pgEx.Shape,pg.Shape);
            pgout=plot(psout);
            setmasc(pgout,getmasc(pgEx) & ~getmasc(pg));
    end
        % the previous ROI of the index provides its color
    
        % except if forced
    index0=getappdata(hf,'index0');
    if ~isempty(index0) && ipgEx==index0
        paff0=getappdata(hf,'paff0');
        if ~isempty(paff0)
            paff=paff0; % paff fixed by variables
        end
    end
    setpaff(pgout,paff);
    setindex(pgout,getindex(pgEx));
    %%%

    setnomRoi(pgout,getnomRoi(pgEx)); %the initial name
    
    
end
delete(pgs);

nb=numel(intersect(ipg,ipgs));
if nb %if pg on an occupied index, pg replaced by pgout, the fusion
    delete(pg);  % ...he has to be removed
else  % else ,there is no pgout for this index
    
    index0=getappdata(hf,'index0');
    if ~isempty(index0) && ipg==index0
        paff0=getappdata(hf,'paff0');
        if ~isempty(paff0)
            
            setpaff(pg,paff) %%%;
        end
        
    end
    nomRoi=getnomRoi(pg);
    index=getindex(pg);
    if isempty(nomRoi)
        nomRoi=['ROI ',num2str(index)];
        setnomRoi(pg,nomRoi);
    end
    %%%
    
    
end


end


