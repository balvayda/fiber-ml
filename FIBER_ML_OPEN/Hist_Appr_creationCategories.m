function [nomC,coulC]=Hist_Appr_creationCategories(n,hf)
% create classes objects(categories)= [mame, color] pairs
% n : number of classes to define = 5 by default in Fiber-LM
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.


if ~exist('hf','var') || isempty(hf)
    hf=figure;
end
set(0,'currentFigure',hf);
for i=1:n
    h(i)=testGraphique(i,n+1,hf);
end

hfin=uicontrol('style','pushbutton','string','valider','units','normalized','position',[0.83 0.02 0.12 0.5/(n+1)]);
set(hfin,'callback',{@close})
waitfor(hfin,'tag','fin');


cmp=0;
for i=1:n
    if get(h(i).hcheck,'value')>0
        cmp=cmp+1;
        nomC{cmp}=get(h(cmp).hnom,'string');
        coulC(cmp,:)=get(h(cmp).hcoul,'BackGroundColor');
    end
    delete(h(i).hcheck);pause(0.05);
    delete(h(i).hpanel);pause(0.05);
    
end

delete(hfin);
set(hf,'visible','off');
        

function [hh,hf]=testGraphique(i,n,hf)

set(0,'CurrentFigure',hf);
pas=1/n;
h=1-pas*i;      % height of the line in the image
hh.hpanel=uipanel('Title',['classe n� ', num2str(i)],'position',[0.15 h 0.85 pas]);
hh.hcheck=uicontrol('style','checkbox','units','normalized','position',[0.01 h 0.05 0.7*pas]);
set(hh.hcheck,'value',1,'callback',{@display,hh.hpanel});

hh.hnom=uicontrol('style','edit','string','name ?','parent',hh.hpanel,'units','normalized','position',[0.01 0.02 0.7 0.95]);
set(hh.hnom,'Callback',{@validName});

hh.hqcoul=uicontrol('style','pushbutton','string','color ?','parent',hh.hpanel,'units','normalized','position',[0.73 0.02 0.12 0.95]);
hh.hcoul=uicontrol('style','text','parent',hh.hpanel,'units','normalized','position',[0.88 0.02 0.1 0.95]);

set(hh.hqcoul,'Callback',{@selectColor,hh.hcoul});
function display(ici,~,hp)
valeur=get(ici,'Value');
if valeur>0
    set(hp,'Visible','on');
else
    set(hp,'visible','off');
end

function validName(ici,~)
str=get(ici,'string');
set(ici,'string',regexprep(str,'\W*','_'));

function selectColor(~,~,hcoul)
%disp('d');
try
    couleur=uisetcolor(hcoul,'Couleur de la cat�gorie');
    set(hcoul,'BackgroundColor',couleur);
catch
    errordlg('Pb uisetColor');
end

function close(ici,~)
set(ici,'tag','fin');