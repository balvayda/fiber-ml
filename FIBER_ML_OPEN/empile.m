function mat=empile(mat,masc)
% all pixels provided in a 2D list (npixels x ndimOfPixels), whether the pixel has one or more
% dimensions (gray, color, time signals)
% IN
% mat : matrix of data
% masc : masc to avoid outside pixels

% D Balvay 2 decembre 2008
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

if ndims(mat)==2
    if exist('masque','var') && ~isempty(masc)
        mat=mat(masc>0);
    else
        mat=mat(:);
    end
    
else
    s=size(mat);
    nt=s(end);      % dimension time
    s=s(1:end-1);  % dimension space

    mat=reshape(mat, [prod(s) nt]);
    if exist('masc','var') && ~isempty (masc)
        masc=masc(:);
        mat=mat(masc~=0,:);
    end
end
