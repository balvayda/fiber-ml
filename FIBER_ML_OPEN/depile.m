function [mat2,loc]=depile(mat,masc,externe,cutof,dim)
% inverse function of empile. 
% reorders the stacked pixels to their original place in the image or image stack
% according to a masc if any. 
% From D Balvay : 2 december 2008
% IN
% mat : data to store
% masc : binary masc or empty
% externe : if masc, the value outside the masc
% cutof : if ==1 : crop the data around the masc
% dim : force the number of dimensions
% for FIBER-LM1.0 D.Balvay 07/01/2022 PARCC U970 Inserm/Univ. Paris, France.

if exist('cut','var') && ~isempty(cutof) && ~isequal(cutoff,'cut')
    [masc,loc]=cropBW3(masc,masc);
end
if exist('dim','var')
    s=ones(1,dim);
    s(1:ndims(masc))=size(masc);
    nt=numel(mat)/sum(masc(:));
else
    s=size(masc); %  spatial dimension
    nt=size(mat,2); % time dimension 
end



if ~exist('externe','var') || isempty(externe)
    mat2=zeros([prod(s) nt],class(mat));           % zero filling
else if isequal(externe,'nan')
        mat2=nan([prod(s) nt],class(mat));           % NaN filling
    else
        mat2=externe*ones([prod(s) nt],class(mat));    % 'externe' assumed to be a scalar
    end
end

if  ~isempty (masc)
    masc=masc(:);
    mat2(masc~=0,:)=mat;
end
clear mat
mat2=reshape(mat2, cat(2,s,nt));

