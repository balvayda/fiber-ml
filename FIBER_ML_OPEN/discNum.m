function sn = discNum(filename,fixedValue)
% write filename (sn.mat) with the partial ID of C:/ indicating that the
% licence has been checked on this computer. Tested in FIBER.mlapp

% for FIBER-LM1.0 D.Balvay PARCC U970 Inserm/Univ. Paris, France.

% 
% Computer Id from a part of the C:/ disc number (for Windows only)
% Only part of the true disc Id is removed to avoid a trace of "personal data" in
% files if copied in a new computer
% IN : 
%   filename (option) mat filename where to save the Id
%   fixedValue : Id fixed and not get from the computer

% if an unexpected error occurs (file permission .. sn='2222')
try
    if nargin<2 
        cmd = 'wmic diskdrive get SerialNumber';
        [~, result] = system(cmd);
        %// Extract first hard disk serial number
        fields = textscan( result, '%s', 'Delimiter', '\n' );
        fields = strtrim(fields{1});
        sn = fields{2}; % serial number
            % keep half number 
        sn = split(sn,'_');
        if numel(sn)>1
            sn = join(sn(1:2),'_');
            sn = sn {1};
        else
            sn = sn{1};
            sn = sn(1:5);
        end
    else
        sn = fixedValue;
    end
    
    % write file
    if nargin>0 && ~isempty(filename)
        save(filename,'sn');
    end
catch
    sn = 'ERROR';
end


