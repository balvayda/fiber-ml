function [reps,fichs]=dirq(repertoire,filtre)
% altenative to dir (faster)
 % for FIBER-LM1.0 D.Balvay PARCC U970 Inserm/Univ. Paris, France.
% from an old free code with a lost origine

if ~exist('filte','var') || isempty(filtre)
    filtre=''; % no filtering by default
end

disp(repertoire)
jFile = java.io.File(repertoire); %java file object
jPaths = jFile.listFiles(filtre);       %java.io.File objects
jNames = jFile.list(filtre);            %java.lang.String objects
isFolder = arrayfun(@isDirectory,jPaths); %boolean
reps = cellstr(char(jNames(isFolder)));   %cellstr
fichs = cellstr(char(jNames(~isFolder)));   %cellstr

fichs((cell2mat(strfind(fichs,'.'))==1))=[]; % no hidden files
reps((cell2mat(strfind(reps,'.'))==1))=[];  % no hidden directories
 
  % no hidden files

N=numel(fichs);
if N==1 && isequal(fichs{1},'')
    fichs={};
end

M=numel(reps);
if M==1 && isequal(reps{1},'')
    reps={};
end
