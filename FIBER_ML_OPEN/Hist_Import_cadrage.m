function [rect,hf,index]=Hist_Import_cadrage(classe,modeMan,mode,valeur,trace)
%[rect,hcad,index]=cadrage(imk,'manuel',mode,valeur,trace);
% for FIBER-LM post1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.
% IN
% classe :  carte de classification de l'image de l'image en basse
%           r�solution pour calcul et trac� de rectangles
% modeMan : mode manuel (1) /auto (0), manuel = contr�le et correction
%           manuelle
% mode :    d�finition de l'�chelle {'taille'(definition),'resolution','valeur'(index)}
% valeur : valeur d'�chelle ou d'index recherch�e
% trace : recuperation champs de l'image de la couche identifi�e
% OUT
% rect : liste des positions de chaque rectangle � l'�chelle trac�e
% hf : object graphique figure (??)
% index : index ou valeur choisie dans l'interface graphique

nomTotal=trace.info(1).Filename;
[~,nomfich,~]=fileparts(nomTotal);  % nom du fichier en cours de traitemeny

% 1/ Calcul des rectanges incluant les objets sombres"
cls=unique(classe);
masque=classe<cls(end);
masque=imfill(masque,'holes');
k=20;
ker=1/k/k*ones(k,k);
masque=conv2(masque,ker,'same')>0.5;
masque=bwareaopen(masque>0,round(numel(masque)/500));
masque=imdilate(masque,ones(15,15));
stat=regionprops(masque,'BoundingBox');

% 2/ liste des valeurs accessibles pour le mode s�lectionn� pour toute la
%       pile d'images
switch mode
    case 'taille'
        Vs = trace.taille;
    case 'resolution'
        Vs = trace.resolution;
    otherwise
        Vs=valeur;  % cas mode='numero'
end

ns=numel(Vs);
nomVs=cell(ns,1);  

for i=1:ns  % version num�rique des valeurs
    nomVs{i}=num2str(Vs(i));
end

ival=find(Vs<=valeur);
vmax=max(Vs(ival));
ival=find(Vs==vmax,1,'First'); % la plus grande valeur inf�rieure ou �gale � la valeur de r�f�rence (valeur = r�solutionmax)

for i=1:numel(stat)             % nombre de rectangles � tracer
    tmp=stat(i).BoundingBox;
    rect(i).position=tmp;
    rect(i).nom=['R',num2str(i)];
end
if exist('modeMan','var') && isequal(modeMan,'manuel') % a priori on est en mode manuel
    % trace de l'image de fond
    hf=figure;
    setappdata(hf,'cmp',1); % compteur de creation de region
    ha=axes(hf,'units','normalized','position',[0.05 0.1 0.90 0.80]); 
    imagesc(classe); axis image;
    setappdata(hf,'ha',ha);
    ylabel(nomfich);
    % trace des differents rectangles + menus de modification
    for i=1:numel(stat)
        r(i) = images.roi.Rectangle(gca,'Label',rect(i).nom,'Color',[1 0 0],'Position',stat(i).BoundingBox);
        cmp= getappdata(hf,'cmp');
        set(r(i),'tag','rectangle','UserData',cmp);
        setappdata(hf,'cmp',cmp+1); % incr�ment du compteur de cr�ation
        ajouteMenu(hf,r(i));
    end
    % ajout des boutons d'action
    setappdata(hf,'r',r);
    uicontrol(hf,'Style','pushbutton','Units','normalized','Position',[0.88 0.01 0.08 0.08],...
        'string','Valider','Callback',{@valider,hf});
    
    uicontrol(hf,'Style','text','Units','normalized','Position',[0.68 0.11 0.11 0.08],...
        'string',mode); 
    
    he=uicontrol(hf,'Style','popupmenu','Units','normalized','Position',[0.68 0.01 0.18 0.08],...
        'string',nomVs,'Callback',{@sca,hf,Vs},'value',ival); %%%%%%%%%
    
    uicontrol(hf,'Style','pushbutton','Units','normalized','Position',[0.58 0.01 0.08 0.08],...
        'string','RAZ','Callback',{@RAZ,hf});
    
    uicontrol(hf,'Style','pushbutton','Units','normalized','Position',[0.48 0.01 0.08 0.08],...
        'string','Ajouter','Callback',{@Ajouter,hf});
    
    % attente de validation pour quitter l'interface graphique
    waitfor(hf,'tag','fermer');
    r=getappdata(hf,'r');
    clear ('rect');
    for i=1:numel(r)
        tmp=r(i).Position;
        rect(i).position=tmp;
        tmp=r(i).Vertices;
        tmp=[min(tmp);max(tmp)]';
        rect(i).box={tmp(1,:),tmp(2,:)};
        rect(i).nom=r(i).Label;
    end
    switch mode
        case {'taille','resolution'}
            index = he.Value;
        case 'numero'
            index=valeur;
    end
    
    %close (hf)
end


function valider(~,~,hf)
set(hf,'tag','fermer');

function RAZ(~,~,hf)
hr=findobj(hf,'tag','rectangle');
delete(hr);
setappdata(hf,'r','');

function Ajouter(~,~,hf)
r=getappdata(hf,'r');
ha=getappdata(hf,'ha');
set(0,'CurrentFigure',hf);
ri= drawrectangle(ha,'Label','Nouvelle Region','Color',[1 0 0]);

cmp= getappdata(hf,'cmp');
set(ri,'tag','rectangle','UserData',cmp);
setappdata(hf,'cmp',cmp+1); % incr�ment du compteur de cr�ation
ajouteMenu(hf,ri);
r=cat(2,r,ri);
setappdata(hf,'r',r);

function renomme(~,~,hf,hr)

r=getappdata(hf,'r');
noms={r.Label};
cmpi=get(hr,'UserData');  % id du rectangle activ�
id=[r.UserData];        % liste des id des rectangles
index=find(id==cmpi,1,'first');

prompt = {'New Name'};
dlgtitle = '---';
dims = [1 35];
definput = {hr.Label};
answer = inputdlg(prompt,dlgtitle,dims,definput);
noms{index}=answer{1};
U = matlab.lang.makeUniqueStrings(noms,index);
set(hr,'Label',U{index})


r(index)=hr;
setappdata(hf,'r',r);

function supprime(~,~,hf,hr)
r=getappdata(hf,'r');
cmpi=get(hr,'UserData');  % id du rectangle activ�
id=[r.UserData];        % liste des id des rectangles
index=find(id==cmpi,1,'first');
r(index)=[];
delete(hr);
setappdata(hf,'r',r);


function ajouteMenu(hf,r)
c=uicontextmenu;
set(r,'UicontextMenu',c,'tag','rectangle');
uimenu('Parent',c,'Label','Change Name','Callback',{@renomme,hf,r});
uimenu('Parent',c,'Label','Supprime','Callback',{@supprime,hf,r});


function sca(ici,~,hf,T)
disp(ici.Value)


