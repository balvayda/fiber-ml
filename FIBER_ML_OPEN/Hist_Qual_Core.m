function [cls]=Hist_Qual_Core(im,him,interface,hroi,listApp,classif,cls)
% visual control and annotations of live classification maps
% IN :
% im=original histological image  
% him=image object were im is graphically available
% interface : structure .trans 0:1/map0:1/com0:1:2, insersion interface
% 	trans: transparency / map: choice of the map (class or quality) / com:
% 	comments
%   0: inactif / 1: actif / 2: activé
% listApp (option) : learning list from Hist_appr_Lanceur
% classif (option): stucture of classification provided by Hist_appr_Lanceur
% cls (option) : classification model (Discriminant analysis) previously
% fitted to the learning list

% OUT :
% cls (Option): classification model (Discriminant analysis) newly
% fitted to the learning list (useful 1 time for the first loop and passed
% a variable for the following iterations

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% interface button bar to insert in the figure?
if ~exist('interface','var') || isempty(interface)
    inser=0;
else
    inser=1;
end


%  Option 1 : if processed in ROIS (regions of interest)
if exist('hroi','var') && ~isempty(hroi)
    % catch a crop of the ROI 
    if isprop(hroi.pg,'Vertices')
        pos=hroi.pg.Vertices;
    else
        pos=hroi.pg.Position;
    end
    mini=ceil(min(pos,[],1));
    maxi=floor(max(pos,[],1));
    cadre=[mini;maxi];
else
    cadre=[];
end

% OPTION 2 : the model was or not previously fitted to the learning list so do
% it if required
% 
if ~exist('classif','var') || isempty(classif)
    if exist('cls','var') && ~isempty(cls)
        [classif,cls]=Hist_Class_Core(im,hroi,listApp,cls);
        
    else
        [classif,cls]=Hist_Class_Core(im,hroi,listApp);
    end
end

% process image to display with overlay
% class map
gamma=1;                            % opacity of the map

nbc=numel(listApp.classeDef.nom);   % how many classes
map=classif.map.brut.im;            % parametric map
colmap=classif.map.brut.colmap;     % colormap associated with this map
limits=[1 nbc];                     % range

over(1).colmap=colmap;
over(1).Limits=[0 1];               % 
over(1).im=insertMap(im,map,gamma,colmap,cadre,limits);
over(1).Label=classif.map.brut.nom;
over(1).Ticks=linspace(0,1,nbc);
over(1).TickLabels=listApp.classeDef.nom;


% preparation
prob=sort(classif.map.prob.im,3,'descend');

% maps of max proba
map=prob(:,:,1);
colmap=jet;
limits = [.5 1];
%nc=size(colmap,1);
ticks=linspace(0,1,5);

over(2).colmap=colmap;
over(2).Limits=limits;
over(2).im=insertMap(im,map,gamma,colmap,cadre,limits);
over(2).Label='decision probability';
over(2).Ticks=ticks;
over(2).TickLabels=strsplit(num2str(ticks));

% ambiguity map
map=(prob(:,:,1)-prob(:,:,2))./(prob(:,:,1)+prob(:,:,2));
colmap=hsv;
limits = [.5 1];
%nc=size(colmap,1);
ticks=linspace(0,1,5);


over(3).colmap=colmap;
over(3).Limits=limits;
over(3).im=insertMap(im,map,gamma,colmap,cadre,limits);
over(3).Label='unambiguousness';
over(3).Ticks=ticks;
over(3).TickLabels=strsplit(num2str(ticks));


ha=him.Parent;
hf=ha.Parent;
setappdata(him,'overlay',over);
manageAction(0,[],hf,ha,him,interface);  % updade action activation

if inser  %  action from the keyboard
    set(hf,'KeyPressFcn',{@manageAction,hf,ha,him,interface});
    
    set(ha,'units','normalized');
    pos=get(ha,'outerposition');
    marge=.1;
    if pos(2)<marge % space for the action bar
        pos2=pos(2);
        pos(2)=marge;
        pos(4)=pos(4)-marge+pos2;
        set(ha,'outerposition',pos);
    end
    
    
    
    % create buttons
    if interface.com>0
        % creation of the buttons in the group
        hg=uibuttongroup(hf,'units','normalized','position',[0 0 1 .1]);
        hstop=uicontrol('parent',hg,'units','normalized','position',[.01 .1 .1 .8],'string',' Stop ');

        hup=uicontrol('parent',hg,'units','normalized','position',[.2 .1 .1 .8],'string','map+(^)');
        hdown=uicontrol('parent',hg,'units','normalized','position',[.3 .1 .1 .8],'string','map-(v)');

        hleft=uicontrol('parent',hg,'units','normalized','position',[.5 .1 .1 .8],'string','hide (<)');
        hright=uicontrol('parent',hg,'units','normalized','position',[.6 .1 .1 .8],'string','show (>)');

        hform=uicontrol('parent',hg,'units','normalized','position',[.7 .1 .1 .8],'string','form+ (n)');

        hok=uicontrol('parent',hg,'units','normalized','position',[.9 .1 .1 .8],'string',' OK ');

        % an action for each button
        if interface.map>0
            set(hup,'callback',{@action,'uparrow',hf,ha,him,interface});
            set(hdown,'callback',{@action,'downarrow',hf,ha,him,interface});
        else
            set(hup,'visible','off');
            set(hdown,'visible','off');
        end

        if interface.map>0
            set(hleft,'callback',{@action,'leftarrow',hf,ha,him,interface});
            set(hright,'callback',{@action,'rightarrow',hf,ha,him,interface});
        else
            set(hleft,'visible','off');
            set(hright,'visible','off');
        end
        if interface.com>0
            set(hform,'callback',{@action,'n',hf,ha,him,interface});
        else
            set(hform,'visible','off')
        end

        if interface.close>0
            set(hok,'callback',{@action,'return',hf,ha,him,interface});
            set(hstop,'callback',{@action,'escape',hf,ha,him,interface});
        else
            set(hok,'visible','off');
            set(hstop,'visible','off');
        end
    end
end


function action(ici,~,cle,hf,ha,hi,interface)
manageAction(ici,cle,hf,ha,hi,interface)



function manageAction(~,event,hf,ha,hi,interface)

% 
if ~exist('interface','var') || isempty(interface)
    inser=0;
else
    inser=1;
end



gamma=1;

over=getappdata(hi,'overlay');
nbIm=numel(over); % number of maps (overlays)

numIm=getappdata(hi,'numIm');
if isempty(numIm)
    numIm=1;
end
if ~isempty(event)
    if isprop(event,'Key')
        k=event.Key; % keyword action
    else
        k=event;  % current argument
    end
else
    k='rien';
end
% display settings
% choice of the user

note = 0; % by defaut no form visible for the user
switch k
    case 'leftarrow'  % diplay/hide the map if activated
        if interface.trans~=0
            gamma=0;
        end
    case 'rightarrow'
        if interface.trans~=0
            gamma=1;
        end
    case 'uparrow'  % change the map if activated
        if interface.map~=0
            numIm=min(numIm+1,nbIm);
        end
    case 'downarrow'
        if interface.map~=0
            numIm=max(numIm-1,1);
        end
    case 'n'
        if interface.com~=0
            note=1; % activation if not nul
        end
    case 'rien'     % not active: for batch processing
        if inser && interface.com==2
            note=1;  % form activated without keyboard action
        end
    case {'return','space'}
        if interface.close
            set(hf,'Tag','ok');
            hn=getappdata(hf,'hnotif');
            if ~isempty(hn)
                close(hn);  % close the form if present
                rmappdata(hf,'hnotif');
            end
            return;
        end
    case {'escape'}
        if interface.close
            set(hf,'Tag','stop');
            return;
        end    
    otherwise
        disp(k);
end



% display image
over=over(numIm);
imover=over.im;
im=getappdata(hi,'im');
ha.Colormap=over.colmap;
if gamma>0
    set(hi,'CData',imover);
else
    set(hi,'CData',im);
end
% display color code
c=colorbar(ha);
c.Label.String=over.Label;
c.Limits=over.Limits;
c.Ticks=over.Ticks;
c.TickLabels= over.TickLabels;
if note>0
    % manual form for annotations
    set(hf,'units','normalized');
    p=get(hf,'Position'); % position of the principal figure
    hn=figure('units','normalized','position',[p(1)+p(3)+0.01 p(2) .15 p(4)]); % fenetre de notification
    set(hn,'MenuBar','none','ToolBar','none','name','Form');
    % validation
    top=.8;
    
    uicontrol('Style','text','String','Status',...
        'units','normalized','position',[.01 top .35 .07]);
    validS={'?','OK','Redo','Exclusion'};
    hs.valid=uicontrol('Style','popupmenu','String',validS,...
        'units','normalized','position',[.1 top-0.2 .8 .22]);
    
    % quality image
    scoreS={'?','5','4','3','2','1'};
    score=[0 5 4 3 2 1];
    top=.6;
    uicontrol('Style','text','String','Image score',...
        'units','normalized','position',[.05 top .4 .07]);
    hs.qualim=uicontrol('Style','popupmenu','String',scoreS,...
        'units','normalized','position',[.1 top-0.2 .8 .22]);
    
    % quality classification
    top=.45;
    uicontrol('Style','text','String','Segmentation score',...
        'units','normalized','position',[.05 top .55 .07]);
    hs.qualcl=uicontrol('Style','popupmenu','String',scoreS,...
        'units','normalized','position',[.1 top-0.2 .8 .22]);
    
    % free note (1 line)
    top=.25;
    uicontrol('Style','text','String','free note',...
        'units','normalized','position',[.05 top .4 .07]);
    hs.notif=uicontrol('Style','edit',...
        'units','normalized','position',[.1 top-.2 .8 .22]);
    
    envoi(0,0,hf);  % by default the note is void
    set(hs.valid,'Callback',{@envoi,hf,hs,validS,score});
    set(hs.qualim,'Callback',{@envoi,hf,hs,validS,score});
    set(hs.qualcl,'Callback',{@envoi,hf,hs,validS,score});
    set(hs.notif,'Callback',{@envoi,hf,hs,validS,score});
    if interface.close % possibilité to close the main figure from the form
        set(hn,'KeyPressFcn',{@finir,hf});
    end
    setappdata(hf,'hnotif',hn);
else
    hn=[];
end

% write graphical variables
setappdata(hi,'gamma',gamma);
setappdata(hi,'numIm',numIm);


function envoi(~,~,hf,hs,validS,score)
% send the comment to hf 

if exist('hs','var')
    comment.valid=string(validS{get(hs.valid,'value')});
    comment.image=string(score(get(hs.qualim,'value')));
    comment.classif=string(score(get(hs.qualcl,'value')));
    comment.autre=string(get(hs.notif,'string'));
else
    comment.valid="";
    comment.image="";
    comment.classif="";
    comment.autre="";
end
setappdata(hf,'comment',comment);

function finir(~,event,hf)
k=event.Key;
switch k
    case {'space','return'}
        set (hf,'Tag','fermer');
end



