function Hist_env_build(env)
% build a context of work defined by a study, a user and a reading
% "context" means a set of directories where (data and derived data are read
% an written
% (not the graphical interface, just the directories

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

reps=env.Reps; % a structure indicating the names of all the key directories for the chain of analysis



% learning step 
createRep(reps.AppData);     % sample of images to teach the classifier (learning stage)
createRep(reps.AppLabel);    % labels / classes
createRep(reps.AppList)      % learning lists
% production step / from study data
createRep(reps.ProdData)     % study data to analyse in the production stage
createRep(reps.ProdRoi)      % opional regions of interest
createRep(reps.ProdRes)      % results, image per image
% prod+ : images and tab
createRep(reps.ProdResFig)   % associated classification maps
createRep(reps.ProdResTab)   % synthesis table counting the pixels of each class for all the images analyzed during the batch
% quality
createRep(reps.QualProd)     % quality control per image
createRep(reps.QualProdTab)  % summary

function createRep(rep) % create the directory if not previously created
if ~isfolder(rep)
    mkdir(rep);
end
