

function Hist_Appr_Lanceur(env)
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% The learning interface
% To do, change the file name (fun name) by Hist_Learn_Launcher

% Hist_Appr_Lanceur(env)
% "Appr" for apprentissage = learning
% "Lanceur" = Launcher
% main code for the learning stage of fiber
% IN:
% env : environnement provided by fiber, from Hist_env_default() or Hist_env_Edit;
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

 % working directories
 

directories.donnee=env.Reps.AppData;
directories.label=env.Reps.AppLabel;
directories.listePixel=env.Reps.AppList;
 
% initialisation
im=imread('origines.jpg'); % interal image (not functional)
s=load('DefLabel.mat');     % internal labels (not functional)
coulC=s.rangement.classeDef.couleur; 
nomC=s.rangement.classeDef.nom;
clear s;

% creation current main figure with pictures, pixel-class selection, and
% other action-buttons
h=figure('Color',[0.6 .6 0.6],'Toolbar','figure','name','Learning','NumberTitle','off');
set(h,'units','normalized','position',[0.0063    0.0444    0.6797    0.8778 ] );
set(h,'MenuBar','none','ToolBar','none');
    
% figure's variables 
setappdata(h,'pixels',[]);  % void RGB liste 
setappdata(h,'classes',[]); % void class/label list
setappdata(h,'coulC',coulC);
setappdata(h,'iClass',1);
setappdata(h,'nomC',nomC);
setappdata(h,'nomImage',[]);
setappdata(h,'reps',directories);
[~, repIci]=system('cd'); %% WARNING WINDOWS ONLY
setappdata(h,'repProg',repIci(1:end-1)); % remove the return


% second figure to display the learning list [RGB-class] graphically in a
% trichrome space
h2= figure('name','Learning List','NumberTitle','off','MenuBar','none');
set(h2,'units','normalized','position',[0.6932    0.5333    0.2917    0.3889] );
setappdata(h,'h2',h2);
Hist_Appr_activScatter3D([],[],coulC,h2);

set(h2,'toolbar','figure');
set(h2,'menubar','none');

% third figure to display optionally the classes/labels to create
h3=figure('units','normalized','position',[0.6943    0.0463    0.2917    0.4028],'visible','off','name','Definition Labels','NumberTitle','off');
setappdata(h,'h3',h3);          
set(h3,'MenuBar','none');

% positioning
set(0,'currentFigure',h); % back on the main figure
% axe to display the class colors (coulC) - which are active to define the
% activated class
ha1=axes('units','normalized','position',[0 0 0.1 0.99]); % for the class / label
ha2=axes('units','normalized','outerposition',[0.10 0.03 0.9 0.90]); % for the image
set(ha2,'XTick',[]);set(ha2,'YTick',[]);

%xlabel('clic on left panel to change the current label')
axis image;
ha1.Interactions=[];
ha1.Toolbar.Visible='off';


actualiseCategorie(h,ha1,ha2);
actualiseImage(im,[],h,ha2,h2); 



% for debugging only
hh=uicontrol('style','pushbutton','string','Info',...
    'units','normalized','position',[0.2 0.91 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@informer,h});

% action buttons
% block-1 label/class management + select the current image
hh=uicontrol('style','pushbutton','string','Read Labels',...
    'units','normalized','position',[0.2 0.048 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@lireLabel,h,ha1,ha2,h2});

hh=uicontrol('style','pushbutton','string','Create Labels',...
    'units','normalized','position',[0.2 0.01 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@defLabel,h,ha1,ha2,h2,h3});

hh=uicontrol('style','pushbutton','string','Read Image',...
    'units','normalized','position',[0.3 0.048 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@readAnImage,h,ha1,ha2,h2});

hh=uicontrol('style','pushbutton','string','Save Labels',...
    'units','normalized','position',[0.3 0.01 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@sauverLabel,h});


% block-2 management of the learning list : create, save...

hh=uicontrol('style','pushbutton','string','Read List',...
    'units','normalized','position',[0.5 0.048 0.1 0.042]);
set(hh,'callback',{@lireClassRef,h,ha1,ha2,h2});

hh=uicontrol('style','pushbutton','string','Remove Points',...
    'units','normalized','position',[0.6 0.048 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@remoPoint,h,h2});

hh=uicontrol('style','pushbutton','string','Reset',...
    'units','normalized','position',[0.5 0.01 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@initialiserPix,h,h2});

hh=uicontrol('style','pushbutton','string','Save List',...
    'units','normalized','position',[0.6 0.01 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@saveList,h});


% block-3 Loop quality control and exit

hh=uicontrol('style','pushbutton','string','Control',...
    'units','normalized','position',[0.8 0.048 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@controler,h});

hh=uicontrol('style','pushbutton','string','Close',...
    'units','normalized','position',[0.8 0.01 0.1 0.042],...
    'SelectionHighlight','off','interruptible','off');
set(hh,'callback',{@closeAppli,h,h2,h3});

set(0,'currentFigure',h);



function selectCoul(~,~,hf,ha,ha2) % select the current class by clicking in its color in the colobar

coulC=getappdata(hf,'coulC');
nomC=getappdata(hf,'nomC');
cp=get(ha,'CurrentPoint');
nbC=round(cp(1,2));          % current class number
coulCC=coulC(nbC,:);         % color of this class
set(hf,'color',mean([coulCC; 1 1 1]));     % anti-oops about the current class which is activated
set(hf,'CurrentAxes',ha2);
title(['Clic  pixels for the class : ',nomC{nbC}]);
pause(0.1);
setappdata(hf,'iClass',nbC);  % save in the figure's variables;

function selectPoint(hi,~,h,h2,ha)

cp=get(ha,'CurrentPoint');
cp=round(cp(1,1:2));          % current position (localisation pix image)
%disp(cp)
im=get(hi,'Cdata');
pix=squeeze(im(cp(2),cp(1),:))';

ic=getappdata(h,'iClass');

pixels=cat(1,getappdata(h,'pixels'),pix);      % add a pixel a la liste
classes=cat(1,getappdata(h,'classes'),ic);     % add the corresponding class

setappdata(h,'pixels',pixels);     % update the figure's variables
setappdata(h,'classes',classes);

actualiseScatter(h,h2);

function remoPoint(~,~,h,h2)
% remove the previous point [RGB-class] in the learning list
% pixels=getappdata(h,'pixels');      % select the set of pixels
% classes=getappdata(h,'classes');     % and the set of classes
% 
% pixels=pixels(1:end-1,:);           % remove the last element 
% classes=classes(1:end-1);
% 
% setappdata(h,'pixels',pixels);     % update  the figure's variables
% setappdata(h,'classes',classes);
askScatter(h);
actualiseScatter(h,h2);

function closeAppli(~,~,h,h2,h3) % exit
% prepare the exit
validation=questdlg('Sure to Close ?',...
    '','yes','no','yes');
if isequal(validation,'yes')
    close(h3);
    close(h2);
    close(h);
end

function rangement=saveList(~,~,h)
% "rangement" = storage
% classes are embeded in the learning list for consistent ulterior display
rangement.pixels=getappdata(h,'pixels');
rangement.classes=getappdata(h,'classes');
rangement.classeDef.nom=getappdata(h,'nomC');
rangement.classeDef.couleur=getappdata(h,'coulC');   
rangement.nomImage=getappdata(h,'nomImage');  % image names associated with the learning (trace)

if nargout==0
    reps=getappdata(h,'reps');
    uisave('rangement',[reps.listePixel,'LearningList']);
end

function initialiserPix(~,~,h,h2)

validation=questdlg('Are you sure you want to delete the learning list?',...
    'confirmation Reset','yes','no','yes');

if isequal(validation,'yes')

    nomImage=getappdata(h,'nomImage');     % remove history of the selected images
    if ~isempty(nomImage)
        nomImage=nomImage(end);
    end
    setappdata(h,'pixels',[]);     % remove the learning list
    setappdata(h,'classes',[]);
    setappdata(h,'nomImage',nomImage);
    
    actualiseScatter(h,h2);

end

function lireClassRef(~,~,h,ha1,ha2,h2)

    % initial values
pixels0=getappdata(h,'pixels');      % select the  pixels list
classes0=getappdata(h,'classes');     % select the classes liste
coulC0=getappdata(h,'coulC');
nomC0=getappdata(h,'nomC');
nomImages0=getappdata(h,'nomImage');

    
    % read   
reps=getappdata(h,'reps');
uiopen([reps.listePixel,'*.mat']);

    % get pixels and classes in the file
pixels=rangement.pixels;
classes=rangement.classes;
coulC=rangement.classeDef.couleur;
nomC=rangement.classeDef.nom;

nomImage=rangement.nomImage;

% add the list in the file to the current list
if ~isempty(pixels0)
    if isequal(coulC0,coulC) && isequal(nomC0,nomC)     % if the lists are in line 
        pixels=cat(1,pixels,pixels0);               % ..fusion
        classes=cat(1,classes,classes0);        % 
        nomImage=cat(1,nomImage,nomImages0);
    end
end

% update the figure's variables
setappdata(h,'pixels',pixels);  
setappdata(h,'classes',classes); 
setappdata(h,'coulC',coulC);
setappdata(h,'nomC',nomC);
setappdata(h,'nomImage',nomImage);

actualiseCategorie(h,ha1,ha2);
actualiseScatter(h,h2);


function readAnImage(~,~,h,ha1,ha2,h2)
% only for tiff images, for proprietary files, see Hist-Import functions if
% they work on your data
reps=getappdata(h,'reps');
cd(reps.donnee);
[fich,rep]=uigetfile({'*.tif','*.tiff'},'Select an image for learning');
nomfichier=[rep,fich];
cdProg(h);
im=imread(nomfichier);
fich=regexprep(fich,'_','');
actualiseImage(im,fich,h,ha2,h2);  % update the main figure
actualiseCategorie(h,ha1,ha2)       % update de graphical view of the learning list

function informer(~,~,h)
getappdata(h)

function defLabel(~,~,h,ha1,ha2,h2,h3)
 % creates labels/class graphically
set(h3,'visible','on');

nc = 5 ; % !!! MAX NUMB OF CLASS, Change if required

[nomC,coulC]=Hist_Appr_creationCategories(nc,h3);
setappdata(h,'pixels',[]);  
setappdata(h,'classes',[]); 
setappdata(h,'nomImage',getappdata(h,'nomfichieractif')); 
setappdata(h,'coulC',coulC);
setappdata(h,'nomC',nomC);
actualiseCategorie(h,ha1,ha2);
actualiseScatter(h,h2);


function actualiseCategorie(h,ha1,ha2) % update display
fond=get(h,'color');
set(h,'color',round(fond/2));
coulC=getappdata(h,'coulC');
nomC=getappdata(h,'nomC');
set(0,'CurrentFigure',h);
[nx,ny]=size(coulC);
set(h,'currentAxes',ha1);
hi1=imagesc(reshape(coulC,[nx 1 ny]));
 box on; 
set(hi1,'ButtonDownFcn',{@selectCoul,h,ha1,ha2}); % action while clicking : the current class
for i=1:numel(nomC)
    ton=mean(coulC,2);
    if ton(i)>0.5
        text(1,i,nomC{i},'color','k','horizontalAlignment','center');
    else
        text(1,i,nomC{i},'color','w','horizontalAlignment','center');
    end
end


set(h,'currentAxes',ha2);
ha1.Interactions=[];
ha1.Toolbar.Visible='off';
ha2.Toolbar.Visible='on';
ha2.XTickMode = 'manual';
title(['Click pixels of the class : ' nomC{1}]);
set(h,'color',fond);

function sauverLabel(~,~,h)

rangement.classeDef.nom=getappdata(h,'nomC');
rangement.classeDef.couleur=getappdata(h,'coulC');   % could be completed

reps=getappdata(h,'reps');

uisave('rangement',[reps.label,'labels.mat']);





function lireLabel(~,~,h,ha1,ha2,h2) % read learning list
reps=getappdata(h,'reps');
uiopen([reps.label,'*.mat']);
setappdata(h,'pixels',[]);  
setappdata(h,'classes',[]); 
setappdata(h,'coulC',rangement.classeDef.couleur);
setappdata(h,'nomC',rangement.classeDef.nom);
setappdata(h,'nomImage',getappdata(h,'nomfichieractif'));  % remove history of the image names used in the learning stage


actualiseCategorie(h,ha1,ha2);
actualiseScatter(h,h2);

function actualiseImage(im,nomfichier,h,ha2,h2) % update the main figure
% 
set(0,'currentFigure',h);

nomC=getappdata(h,'nomC');
him=getappdata(h,'him'); % recovery of the current image if exists
if ~isempty(him)
    set(him,'CData',im);
else
    set(h,'currentAxes',ha2); % else the previous one
    him=imagesc(im);  % show the image
    setappdata(h,'him',him);
    
end
axis image;
setappdata(h,'nomfichieractif',nomfichier);

set(him,'ButtonDownFcn',{@selectPoint,h,h2,ha2});
if ~isempty(nomfichier)
    setappdata(h,'nomImage',cat(1,getappdata(h,'nomImage'),{nomfichier})); %add the image name in the history
end
set(ha2,'XTick',[]);set(ha2,'YTick',[]);

[nx,ny,~]=size(im);
ylim([0 nx]);
xlim([0 ny]);

ylabel(nomfichier);
title(['Clic pixels of the class : ' nomC{1}]);
xlabel('To change the current class, clic on the left panel')
setappdata(h,'himage',him);
setappdata(him,'im',im);
ha2.Toolbar.Visible='on';

function actualiseScatter(h,h2)  % update the scatter plot of the learning list
if ~ishandle(h2)
    h2= figure('name','Learning List','NumberTitle','off');
    set(h2,'units','normalized','position',[0.6932    0.5333    0.2917    0.3889] );
    setappdata(h,'h2',h2);
end
BKcolor=get(h2,'color');
set(h2,'color',BKcolor/2); % darker backGround = " busy "
pause(0.02);
pixels=getappdata(h,'pixels');
classes=getappdata(h,'classes');
coulC=getappdata(h,'coulC'); 


Hist_Appr_activScatter3D(pixels,classes,coulC,h2,h);
set(h2,'color',BKcolor);

function askScatter(h)  % ask for information about  points to remove in the learning list

% variables
pixels=getappdata(h,'pixels');
classes=getappdata(h,'classes');
coulC=getappdata(h,'coulC'); 


toKeep = Hist_Appr_removScatter3D(pixels,classes,coulC,h);

pixels = pixels(toKeep>0,:);
classes = classes(toKeep>0,:);

setappdata(h,'pixels',pixels);
setappdata(h,'classes',classes);


function cdProg(h)

cd(getappdata(h,'repProg'));

function controler(~,~,h) % test the classifier base on the current learnin list on a window
him=getappdata(h,'himage');
im=getappdata(him,'im');  % current image
rangement=saveList(0,0,h);
hdraw = drawrectangle();
hroi.masc=createMask(hdraw,im);
hroi.pg=polyshape(hdraw.Vertices);

% display parameters
insert.trans=1; % enable show/hide overlay buttons < >
insert.map=1;   % enable change parametric map v ^ 
insert.com=0;  % no user rating
insert.close=0; % no close the calling figure [espace][return]

if ~isempty(rangement.pixels) && ~isempty(rangement.classes)
    Hist_Qual_Core(im,him,insert,hroi,rangement)
    xlabel('use arrows to manage overlay');
else
    disp('no sufficient elements to perform the control');
end
delete(hdraw);



        
        