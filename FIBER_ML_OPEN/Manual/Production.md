# 6 -Production

When starting [production] , FIBER-LM asks from which learning list it has to set its decision algorithm. It is possible to work from a single training list or to compare results from several competing lists.

Once the learning list is chosen, the process is automatic:

* The classification model adjusts its parameters on the learning list and assigns each value in the color space to a class. The classifier generalize the training list to the entire color space (see (1) in the following figure).

* Once the fitting have been made, the model can associate any pixel in an image with a class. FIBER-LM chooses the images to be processed and executes the model for all the pixels of these images (see (2)).

* The result is one class map per image, or per region of interest. In addition, the pixels of each category are counted and their numbers are compiled in a summary table (see (2)).


![](.\figure11.png)

> The summary table provided here concerns the images processed during this batch. For a general summary of the entire study, see [Summary](.\Summary.html)



[return to the main page](.\FIBER.html)