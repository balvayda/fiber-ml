# 4 - Learning

This step allows the biologist to fit the classifier (classifier settings) using a natural language. He has to choose a set of pixels and for each of them indicate to which class/category it belongs.

The purpose of the learning module is to build the learning list : a set of associations between pixel colors and classes.

The interface allows the user to : 
* define the categories and their color rendering
* assign a category to each pixel of your choice
* visualize his learning list and check if this list allows the classifier to produce a convincing result, at least locally

## The panels

The learning interface uses three panels.
* The main panel [1] is used to display.
    * a learning image.
    * the categories to which the pixels can belong, on the left.
    * action buttons, at the bottom.
* The Learning List View Panel [2], where each selected pixel association is plotted : 
    * the position of the point in the color space is the color the pixel
    * the color of the point is its class
* The class creation panel [3] if needed.

![](.\figure7.png)

## Manage the classes/categories

In the Fiber interface, the categories are named *Labels*. Labels can be created for immediate use or can be saved and played back as a file for later use. 

Labels are managed in the left button group **A** in the main panel tool-bar [1], see bellow. The [Create Label] button opens panel [3]. This panel allows you to define 5 classes or less, with a name and a representative color. If less than 5 categories are needed, then you should uncheck the left check boxes for the classes which are not filled in, so that Fiber knows that the number of categories is less than 5.

![](.\figure8.png)

When categories are created or read, they are displayed in color in the *left bar* of the main panel.

This bar is interactive: clicking on a label in the bar activates that class.

To know which label is active, just look at the background color of the main panel. As an example, the `CELL` category , in yellow, has been selected in the first figure of the document.

> Use simple names without special characters

## Display an image

The images of the training data are opened with a button in the block **A** of the tool-bar of the main panel [1].

## Manage the learning list

When a category is activated (left bar of the main panel), pixels are associated to this category by clicking in the image.

In order to click in the right place, it is possible to zoom and move the field of view using tools that appear at the top left of the image when you hover over it with the mouse.

Each mouse click adds a tagged pixel to the training list.

If a pixel was selected by mistake, it can be removed from the list using the [Remove Point] button in the block **B** of the tool-bar.

The [Remove Point] button removes the last entered point from the list. It can be used iteratively to remove the last points from the learning list.

The lists can be saved as a file, and read later (Block **B** of the tool-bar). It is also possible to empty the current learning list with a button in this block.

> The learning lists are cumulative. Opening a new learning list adds the read list to the current list. If you want to change the learning list, first remove the current learning list and then read a new one.

The graph of the learning list allows to identify some allocation errors (i.e. a red pixel lost in the middle of the yellow population), and to check if the user has provided enough information to help the machine to define boundaries correctly between classes. 

> Avoiding providing decisions in the most ambiguous regions is like letting the algorithm decide for the user. In practice, this should be avoided to avoid decisions based on  computation side effects. 

## Control the learning list

To control how the learning list is used by the classifier, it is possible to use the [Control] button in the block **C** of the tool-bar.

Click on the button provides the learning list to the classifier and allows to observe how the classifier works on pixels of a region of interest from this list.

In practice, after having clicked on the [Control] button, a rectangle must be drawn in the current figure. Then wait a few seconds.

The pixel intensities in the rectangle are then replaced by the colors of the classes to which they belong.

With the help of the < and > arrows in the keyboard, it is possible to show or hide the class map.

![](.\figure9.png)

To improve the learning, the learning list should be enriched with pixels that are misclassified in the region of interest. This selection can be performed whether the class map is displayed or hidden.

The control tool can be launched iteratively on different regions of the current image and as the learning list grows.

> **Important note**
> The training list is supposed to be representative of the pixels contained in the images to be processed. It is therefore unwise to perform the training on a single image. It is recommended to complete the learning list by successively opening several images of the learning data to be more representative.
>
> Opening a new image does not close the current learning list.

Once the learning list is created, *you must save* it to be able to use it in FIBER-LM. In the following processes the learning lists are **only** accessed by file.

> Learning lists are built up from defined categories. The learning list files therefore contain the labels that were necessary for their constitution. In this way, reading a learning list includes reading the associated classes.

When learning is complete, click [Close].

[return to the main page](.\FIBER.html)