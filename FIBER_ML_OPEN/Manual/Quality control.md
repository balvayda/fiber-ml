# 7 - Quality control

When the [QUALITY] button is clicked, two control windows open.

## Graphical control

The main window provides a visual rendering of the semantic segmentation (1 position = 1 nominal category).

The buttons of the main window : *Graphical control* [1], allows to show [Show] or hide [Hide] this classification map.

The image can be enlarged by using the tools that appear when flying over the image (top right of the image).

The software calculates the most probable categories of each pixel but also an ambiguity map and an information degree map. These maps can be displayed using the [map +] and [map -] buttons.

If the main window is active, the arrow keys can replace the previous buttons.

![](.\figure12.png)

## Form

The right panel (Form [2]) is a *form* that can be filled in or not, filled in completely or partially.

The form can be displayed from the main window again [form +] if it has been closed by mistake.

The fields of the form are as follows

1. **Status**. The values of the status are  `?`,  `OK`,  `redo` and `Exclusion`. Status is a decision to be made about the outcome in the study.
2. **Image score**. The values range from 1 to 5, where 5 represents a natively good quality image and 1 represents a probably unusable image. 0 if not checked.
3. **Segmentation scores**. The values range from 1 to 5, where 5 represents an ideal segmentation and 1 represents an unusable segmentation/classification. 0 if not checked.
4. **Free note**. Free comment without formatting or line breaks

## Overall process

The batch of images to be checked depends on the settings in the Fiber main window. When an image/ROI has been checked, the operator validates the check and a new image is proposed for the next check. To stop the process, click [STOP]. The last control is included.

> The contents of the forms are saved in the form of tables. The most efficient way to collect this information is to perform a summary (the last step)



[return to the main page](.\FIBER.html)