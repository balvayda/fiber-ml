# 8 - Summary



The table of contents provides an overview of the progress of the study. This module provides two tables

1. a scoreboard, which allows for quick identification of what has been done and what remains to be done
2. a detailed table listing the values obtained in terms of results and quality controls

## Scoreboard

For each image of  the study, a step is detected if:
* a ROI has been created (isRoi)
* a segmentation result has been calculated (isRes)
* a quality control has been carried out (isQual).

> The presence of a quality control does not indicate whether the form has been properly completed. If the data has been checked without taking care of the form, the form information will be empty.

![](.\figure13.png)

## Table of results

Each row in the table correspond to a region of interest or an image. If two regions have been drawn, the table will contain two separate rows.

the table contains the following blocks :
* the image information (red)
* the pixel count for each ROI (yellow)
* the outputs of the quality forms if they exist (green)
* a date control to check that the analysis chronology is respected
* a consistency check of the ROI names

![](.\figure14.png)

## Warnings

Normal data analysis should be done in the following order: learning, ROI, production, quality control, summary. However, with Fiber, it is possible to change each step of the process whenever desired.

For example, during quality control, we may realize that we have a problem on some regions of interest. In this case it is possible to list the incriminated images and to intervene specifically on the problematic regions. But be careful, the associated results are not updated automatically.

The warnings in the summary table (gray region), based on an analysis of the dates and the names of the regions of interest, make it possible to identify probable inconsistencies in the process. The user is invited to update the associated treatments in order to have coherent results.

![](.\figure15.png)





​					[return to the main page](.\FIBER.html)

