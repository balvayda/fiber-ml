# 5 -Regions of Interest

It is not uncommon to want to exclude anatomical structures from the images in order to focus the analysis on other structures, or to want to perform analyses on several substructures in the images.

The [ROIS] tool allows you to manage this.

> ROI = Region of interest

When you press the [ROIS] button in the FIBER-LM interface, a new window is open

![](.\figure10.png)

In this window you can perform three types of actions:
* define or modify of the current ROI: block **B** of the tool-bar.
* Create or change the current ROI: bock **A** of the tool-bar.
* Editing the names and colors of the ROI display:
    * edit type: menu
    * edit: legend
    
    > By default ROIs are called 'ROI1', 'ROI2', etc. Rename as needed, with simple names, avoid special characters

## Editing the active ROI

* Take all in the image [all] and delete all [reset].
* Modify by adding [+]
* Modify by a withdrawal [-].

> The regions of interest are exclusive.
> * [+] expands the active region and removes the other ROI from the area, if one of them is there.
> * [-] eliminates any ROI from the area

## Manage multiple ROIs

* To change the active ROI, if several, just select the ROI of your choice in the drop-down list of block **A**.

To create a new ROI, press the [index+] button. Then select the index of the new ROI.

> If the operator selects an already existing index, it simply activates the corresponding ROI.

## Change the appearance of ROI

To change the ROI display, left-click on the name of the ROI in the legend.

You must first choose the action you wish to perform, by right-clicking on the legend. Choose in the menu
* name modification
* color modification

## Validation

If the layout is satisfactory, click [OK]. The drawing is saved as a file.

If the operator wants to stop his ROI plots, he can click [Stop]. In this case, the last plot is ignored.

## Trace its ROI in several times

You can restart [ROI] in the main FIBER window, after having set Processing On >> run>> Untreated to draw ROIs only on images for which ROIs have not be drawn yet.

## Changing the ROI drawing afterward

If the user wishes to resume all his plots from those he has already made, to improve his plots thanks to the intervention of a third person for example, he can restart [ROI] in the main window of FIBER, after having set Processing On >> run>> treated.



[return to the main page](.\FIBER.html)

