

# 2. Define the image data set

The first button of the **Data panel**: to the right of (study data), allows you to select images from a data directory. Images selected in this directory are retrieved in the FIBER-LM directory dedicated to the study data (see [1] in the left panel of the following image).

The second button: to the right of (learn data),  allows to select sample images to copy them into the learning data directory (see [2] in the left panel of the following image).

Alternatively it is possible to point the study data directory directly to a directory already containing data (in the righ panel of the following image).

<img src=".\figure5.png" style="zoom:80%;" />

The image data are assumed to be TIFF images of sufficiently high quality (luminous homogeneity, homogeneity of the staining quality, low or no compression). Most of the information useful for classification is assumed to come from the pixels intensities and not from the spatial structuring of such intensities.

> The module for importing data from manufacturer formats is not integrated yet in version 1.0
>
> Importing in tiff format must be done with care. Poor data will provide poor results.
>
> Click on the images which will be copied on the study and learning data (multiple selection).

[return to the main page](.\FIBER.html)

