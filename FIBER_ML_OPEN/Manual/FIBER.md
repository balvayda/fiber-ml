# FIBER-LM 1.0

## introduction

Fiber is a semantic segmentation and pixel counting software developed for histology. It assumes homogeneous brightness within images and equivalent staining conditions between images processed in a batch (study data).

Data analysis is carried out in two main steps: a *learning phase* where the user defines labels (or *classes*) and assigns these labels to pixels of his/her choice. Then the classifier uses this set of pixel information  (*learning list*) to set a prediction model (*classifier*) that will associate a class to any pixel in the study images. This second stage is the *production phase*. However, there is a third phase in FIBER-LM: the quality control. An analysis is complete if this third phase is performed correctly.

Two user feedbacks control the relevance and limits of the learning. The first feedback (pre-production loop) guides the user in the constitution of his *learning list*. The second loop (post-production loop) allows quality controls to be carried out on all the images of the study or on a random sampling of them.

FIBER-LM records intermediate results to ensure traceability. It has a multi-user data manager. It also allows you to build your analysis progressively (user learning, data enrichment, multiple readings, etc.) without having to restart the whole calculation each time.

FIBER-LM was designed to analyze data with rich color content (targeted staining). Version 1.0 does not use any spatial information to assess the classes. Not targeted staining such as H&E, should not be analyzed with Fiber, but with algorithms that analyze local spatial structures (including texture features).

The  software outputs are: 
- The semantic parametric maps (*class maps*), in tiff format, which can be post-processed, for object counting or shape characterizations.
- Counts of the pixels of each class, for each region of interest of each image, and for all the images of the study, in a synthesis document (table).

FIBER-ML was developed with Matlab 2019a and earlier by Daniel Balvay, PARCC U970 Inserm / Université de Paris / France. Despite the care taken, it is a physicist's programming. Bugs and programming mistakes can therefore be found in FIBER-LM.

## Modules

The different steps to follow are indicated by numbers in the following figure. For more details on each step, click on the corresponding titles.

<img src=".\figure1.png" style="zoom: 50%;" />

1. [Definition of the project](.\Definition of the project.html)

   This section allows you to define the location of the histological images and all derived data. Defining the project (or study), allows Fiber to know where to read the data and where to write the results of each step. Fibers allows the user to define their own workspace for each of their studies or sub-studies.

2. [Deposit of image data](./Deposit to image data.html)

   Here the users select the images (TIF format) of their studies as well as the sample images selected for the learning phase. For more details on each step, click on the corresponding titles.

3. [Choice of data to be processed](.\Choice of data to be processed.html)

   The parameters chosen here are used to select the data to be processed. These parameters allow to target the analysis to sub-populations, which avoids repeating calculations on the data of the whole study, and then to work on sub batches.

4. [Learning](.\Learning.html)

   The learning phase allows classes to be associated with a set of pixels of interest from the learning images (learning data). *This step requires careful user intervention to ensure a quality analysis.* The result of this step is the production of a *learning list* associating pixels to classes according to the indications of the user.

5. [Regions of Interest](.\Regions of Interest.html)

   This module allows you to draw regions of interest on the study images or on some of them. This step is optional. It is possible to define several regions of interest per image.

6. [Production](.\Production.html)

   During this phase, the user chooses a learning list of his choice. The algorithm fits the classifier from this list and analyzes all the images of the study in an automatic way.

7. [Quality control](.\Quality control.html)

   A graphical interface allows the user to evaluate the quality of the classification/segmentation maps and to fill out a visual evaluation form.

8. [Summary](.\Summary.html)

   It collects all the counting results and the quality scores of the study, in a unique table.

## Notes

Before using Fiber we recommend you to read some [NOTES](.\Before you start.html) to improve the use of the software and to avoid some beginner mistakes.

