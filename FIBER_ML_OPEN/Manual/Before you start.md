# Before you start

For research only

## The machine is a type of disciplined student

A learning algorithm will make decisions based on what you have taught it. If the experience you have provided to the algorithm is not appropriate to make a good decision, the result will be hazardous.

In practical terms...

* The learning algorithm being an imitator, if you tend for example to be too sensitive and not specific enough in detecting a biomarker, the classifier will tend to do the same. Bad teacher, bad student. Learning is therefore a critical phase of the process *to be conducted with care and discernment*.
* If you apply the decision algorithm established on images, but you want to apply it to other images, make sure that the staining conditions are comparable. If the staining conditions are too different, it is necessary to *establish a new decision rules (learning) for each staining type*.
* Machine learning generalizes decisions made by the operator during the learning phase. This allows to automate the decision for all pixels of a study. However, the transmission of decision rules to the machine is only valid from a scientific point of view if the machine decisions are based on operator decisions made under comparable conditions (to avoid numerical speculation). It is therefore necessary *to fill the color space* as well as possible with decision elements (a point of the training list is a decision). It is particularly recommended *to establish decisions for ambiguous colors*, for which the machine should not make risky decisions instead of the operator.

## Fiber-ML1.0 is a software in progress

Fiber has several interesting features for the analysis of histological images. However, to date, it is a software developed by a physicist to help biologicial colleagues only.

Bugs may occur,  as few French residues, and several areas of improvement are currently being considered:

* finish cleaning up the code to facilitate understanding by other developers
* predefine the names and colors of the regions of interest in case of multiple ROIs
* manage very large data that may generate memory overflows.
* manage multi-channel images (n >3)
* include import plug-ins to work directly with manufacturer format.

Now, we have to do without these upcoming improvements.

## Multi Users

If several users are running the software, it is advisable to place all the studies of the different users in the same parent directory, for data management and archiving.

It is possible to work with Fiber individually or in a more collaborative way. Several users can work on the same data, compare their learning lists etc.

Analyses performed by experienced operators can be made available to new users as reference material for their training

[return to the main page](.\FIBER.html)