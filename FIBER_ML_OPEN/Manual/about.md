[<img src=".\logoPIV.png" style="zoom:25%;" />](http://piv.parisdescartes.fr/)               [![](.\logoPARCC.png)](http://parcc.inserm.fr/)

[<img src=".\logoINSERM.png" style="zoom:66%;" />](https://www.inserm.fr/)  [<img src=".\logoUDP.jpg" style="zoom:66%;" />](https://u-paris.fr/)

# FIBER-LM 1.0

FIBER-LM is a software proposed to facilitate academic research work. FIBER-LM1.0 has been developed under Matlab 2019b and 2021a, on Windows.

It has been designed and realized by **Daniel Balvay**, engineer in scientific computing at PARCC UMRS 970 Inserm / University de Paris, within the Plateforme d'Imageries du Vivant, in Paris, France.

Contact

* email: daniel.balvay@inserm.fr
* subject email : [FIBER-LM] ...

The evolution of FIBER has been made possible thanks to the active feedback of biologists. Initially from our research unit: **Anaïs Certain**, **Caterina Facchin**, **Clément  Delacroix, Charline Guery**.

Fiber-LM is under the version 1 of the GNU Lesser General Public License. The thumbnails of the buttons are photo-montages performed with free images available on the site of pixabay.

*January 2022*



