

# 3. Choice of data to be processed

The parameters described here are used to adapt analysis as the study progresses.

Ideally, a study follows the main successive steps proposed in FIBER-ML... However, in practice, analyses are often performed by successive approximations.

To avoid restarting all the calculations each time, it is possible to set the process manager. The choices can be modified before typing on the action buttons in the main FIBER-LM window (see 5-7 in the FIBER-ML [introduction](.\FIBER.html)).

## Process on..

> Applies to the [ROIS] and [PRODUCTION] buttons.

* (run) choose the type of data to be processed

    * *untreated*: Only process data, or derived data, for which results are not already available. This option allows you to advance your study by image batches or work sessions.

    * *manual*: perform operations on the images chosen by the user: selection of images (multiple choice) from the list of images in the study. It allows you to perform treatments on a sub-section of the study or to restart analyses only on data that may be problematic.

        > Applies also to [QUALITY]

    * *treated*: Allows you to restart calculations on a sub-population on which you have already performed analyses that you want to correct or improve (after learning-correction or identification of errors in regions of interest). This choice is useful in the initial analysis phase to obtain first user feedback and improve the process on a sampling. For example, it is possible to compare segmentation maps based on different protocols.

         * learning: few points vs. many points; with or without  artifact classes.
        * ROIs: removing areas such as large vessels vs. no removal. 

    * *all*: launch or restart everything, for a treatment that we are sure will be homogeneous. Useful rather at the end of the study to clean up the analysis.

<img src=".\figure6.png" style="zoom:80%;" />

* (ROI required)

    > Applies to [PRODUCTION]

    [ ] -  The classification/segmentation is performed on all available images of the study, whether or not regions of interest (ROI) have been drawn. Useful if the removal of areas concerns some images only.

    [x] - The classification/segmentation is performed only on images for which ROI has been plotted. This is useful to avoid data processing errors when the definition of regions of interest is mandatory, and the plots have not yet been made for all images. This can happen during the progress of the study, or inadvertently if we have forgotten to draw some ROIs.

## Sample for the quality control

> Applies only to the [QUALITY] button.

Post-processing quality control can be performed 

* on *a sample chosen* by the operator: 

    `Processing on>run` = `manual`

    

* *on all results*: 

    `Num / Ratio = `1 

    

* on a *random sample* of results.

    *  a fixed number of results: If `Num  / Ratio` > 1, its value is considered as a number of results to test
    * a fraction of the total results: If `Num  / Ratio` < 1, its value is considered as a ratio. If its value is 0,05 then 5% of the results will be subject to quality control.

Systematic monitoring is more rigorous for moderate populations. Sample-based control is necessary when working on very large amounts of data. This provides a decoupling of scale between human intervention -learning and quality control- and the large masses of data included in the Big Data processing.

​	[return to the main page](.\FIBER.html)

