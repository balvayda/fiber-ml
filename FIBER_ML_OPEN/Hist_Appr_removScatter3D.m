function toKeep = Hist_Appr_removScatter3D(pixs,class,coul,ho)


% displays the pixels classified in the color space (channels)
% AND ask the user to clic on points TO REMOVE them from the learning list

% IN : 
%   pix : list of pixel values (RGB)
%   class : class/label of each pixel in pix
%   coul : list of colors per class
%   ho : option, the figure to activate at the end (give hand back to the main window)

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% 1 / darken classes colors for better visibility vs background
coul2 = rgb2hsv(coul);
coul2(:,2) = sqrt(coul2(:,2));
coul2(:,3) = (0.9*coul2(:,3)).^2;
coul = hsv2rgb(coul2);

% 2 / draw the list of classified pixels (one / one for interactivity)
hf = figure;

[np,nc] = size(pixs); % number of pixels ; number of chanels
keep = ones(1,np); % list of the points to keep
setappdata(hf,'keep',keep); 
last = np;
setappdata(hf,'last',last);

pix=zeros(np,3,'like',pixs); % zero padding if required (if less than 3 Channels)
pix(:,1:nc)=pixs;

for i=1:np
    hp=plot3(pix(i,1),pix(i,2),pix(i,3),'o','MarkerFaceColor',coul(class(i),:),'MarkerEdgeColor','k','PickableParts','all');
    if i==1
        hold on;
    end
    set(hp,'ButtonDownFcn',{@click,hf,i},'UserData',i); % when clicking
end
box on;grid on;
xlabel('Red');ylabel('Green');zlabel('Blue');
set(hf,'Name','Clic on a point to remove it (clic to retrieve)');

% remove or restore last clics
hb = uicontrol('Style','pushbutton','Units','normalized','Position',[.7, 0.02 .08 .05]);
set(hb,'string','<|');
set(hb,'Callback',{@last,hf,-1});


hb = uicontrol('Style','pushbutton','Units','normalized','Position',[.8, 0.02 .08 .05]);
set(hb,'string','|>');
set(hb,'Callback',{@last,hf,1});


% validation (pushbutton)
hb = uicontrol('Style','pushbutton','Units','normalized','Position',[.9, 0.02 .08 .05]);
set(hb,'string','OK');
set(hb,'Callback',{@finir});

% conclude
waitfor(hb,'tag','return');

toKeep=getappdata(hf,'keep'); % get indexes of points to remove

delete(hf);
if exist('ho','var')  % return to the previous active figure
    set(0,'CurrentFigure',ho);
end

function last(~,~,hf,i)

keep = getappdata(hf,'keep');
last = getappdata(hf,'last');

if i<0
     % find the pixel i
    it = findobj(hf,'UserData',last);
    click(it,0,hf,last);
    last = max(1,last-1); % increment
else
    last2 = min(length(keep),last+1);
    if last2>last
        last = last2;
        it = findobj(hf,'UserData',last);
        click(it,0,hf,last);
    else
        %last = maximal value, do nothing
    end
    
end

setappdata(hf,'last',last);

function click(it,~,hf,i)
vis=get(it,'Visible');
keep=getappdata(hf,'keep');
switch vis
    case 'on'
        set(it,'Visible','off')
        keep(i)=0;
    case 'off'
        set(it,'Visible','on')
        keep(i)=1;
        
end
setappdata(hf,'keep',keep);
       
function finir(it,~)

set(it,'tag','return');