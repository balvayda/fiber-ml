function [reps,Id,Access]=Hist_env_default(root,stu,own)
% provides a set of directory names from a parent directory (root) to store all data and data derived from a Fiber
% it defines default subfolders of the analysis corresponding to the
% various steps of the workflow

% IN : 
% root : the parent directory
% study name : defaut or provided by the user
% own : owner or user in the study
% (first reading assumed)
% OUT : 
%reps :structure with the names of the expected directories
%Id : Id of the analysis (work)
%Acces : writes, asking of note a code 
% 
% Note: Hist_Def_Rep() provides the default directories

% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% information concerning acces : public by default
Access.Public=1;
Access.Code=[];


% where the files about environnements are saved

reps.Init=defineRepEnv(); % 
disp('Dir ENV');
disp(reps.Init);
creerRep(reps.Init); % create if it not exist

% read the file indicating the default location of an analysis
if ~exist('root','var') % no root proposed
    if exist('DefLoc.mat','file')
        load('DefLoc.mat','directo');  % default directory for a study without name
    else
        [~,rep]=system('cd'); %% WARNING WINDOWS ONLY
        rep=updir(rep);
        directo=[rep,'\'];
    end
    %if isempty(directo) || ~ischar(directo) || ~exist(directo,'file')
    try
        directo=uigetdir(directo,'select a new directory only if required');
    catch
        directo=uigetdir();
    end
    directo=[directo,filesep];
    save('DefLoc.mat','directo');
    %end
    root=directo;     
    % Iditentier of the work / analysis by default
    Id.STU='Default';
    Id.OWN='Default';
    Id.READ=1;
else
    if ~isequal(root(end),filesep)
        root=[root,filesep];
    end
    % study identification
    if exist('stu','var')
        Id.STU=stu;
    else
        Id.STU='STU_XXX';
    end
    if exist('own','var') % owner
        Id.OWN=own;
    else
        Id.OWN='OWN_XXX';
    end
    Id.READ=1; % reading = 1, change later manually if needed
end


% définition of every sub directory under the parent file including all the
% stages of the analysis in FIBER

% learning phase
reps.AppData=strcat(root,'LearnData\');   % learning data
creerRep(reps.AppData);
reps.AppLabel=strcat(root,'LearnLabel\');
creerRep(reps.AppLabel);
reps.AppList=strcat(root,'LearnList\'); 
creerRep(reps.AppList)
% production phase

reps.ProdData=strcat(root,'ProdData\');  % study data
creerRep(reps.ProdData)
reps.ProdRoi=strcat(root,'ProdRoi\');    % regions of interest
creerRep(reps.ProdRoi)


reps.ProdRes=strcat(root,'ProdRes\');   % results
creerRep(reps.ProdRes)
                                        
reps.ProdResFig=[reps.ProdRes,'Figures\'];% results' figures
creerRep(reps.ProdResFig)
reps.ProdResTab=[reps.ProdRes,'Table\'];    % results' summaries
creerRep(reps.ProdResTab)

% quality control
reps.QualProd=strcat(root,'ProdQual\'); % individual (per image)
creerRep(reps.QualProd)
reps.QualProdTab=[reps.QualProd,'Table\']; % summary
creerRep(reps.QualProdTab)

% all in a variable
env.Id=Id;
env.Access=Access;
env.Reps=reps;
nomf=[reps.Init,nomEnv(env)];

if ~exist(nomf,'file')
    save(nomf,'env');
end

function creerRep(rep) % create directory if not exist
if ~isfolder(rep)
    mkdir(rep);
end


function nom=nomEnv(env) % the file name is defined by the Id of the analysis
nom=[env.Id.STU,'_',env.Id.OWN,'_',num2str(env.Id.READ),'.mat'];
