function Hist_Impor_Lanceur()
% launch importation from propietary format toward tif field at the
% selected scale.
% External function to include in FIBER-LM post1.0

% in this version the directories (input/output) are fixed - to do : change
% it. To change the default directories, change 'Init_import.mat'



s=load('Init_import.mat');

% create default directories if not available
if ~exist(s.rep.dataSource,'dir')
    mkdir(s.rep.dataSource);
end
if ~exist(s.rep.dataImporte,'dir')
    mkdir(s.rep.dataImporte);
end
if ~exist(s.rep.dataTrace,'dir')
    mkdir(s.rep.dataTrace);
end
disp('Valeurs par defaut')
disp('resolution image cadrage (nb pix / mm)')
resPreview=10, % low scale to draw rectangles
disp('resolution image capturee');
resFinal=1000, % high resolution to save the output tif images: (in pixel per mm);
disp('------');

% files to convert from the input directory
[~,fichs]=dirq(s.rep.dataSource,'');
% manual multiple selection in this list
if isempty(fichs)
    fichs=uigetfile([s.rep.dataSource,fichs{1}],'MultiSelect','on');
else
    disp('ERREUR : aucun fichier dans le dossier source');
end


if ~iscell(fichs) % one file : graphical interface with second scale selection
    Hist_Impor_importerCadran(fichs,s.rep,resPreview,'resolution',resFinal);
else                % several
    nf=numel(fichs);
    for i=1:nf
        if i==1 % graphical interface with second scale selection
            [~,resFinal]=Hist_Impor_importerCadran(fichs{i},s.rep,resPreview,'resolution',resFinal);
        else   % scale selection provided by variable (not manually, manually only for the first image)
            Hist_Impor_importerCadran(fichs{i},s.rep,resPreview,'resolution',resFinal);
        end
    end
end
