function [index,res]=Hist_Impor_importerCadran (fichiers,reps,resPreview,mode,valeur)
% 
% External function to include in FIBER-LM post1.0
% Draw rectangles of selection in extended multilevel tif format and export
% them in tifs one level at the choosen scale (several slice in a 

% to do : include a management with blockedImage objects available from
% Matlab 2021

% 1/ small image (scale) to draw rectangles of interest (fields)
close all
nomTot=[reps.dataSource,fichiers];
[im,trace]=Hist_Impor_lireImTiff(nomTot,'resolution',resPreview); %  Hypothesis resolution in cm converted here in pix/mm
index1=trace.index;
disp('Index 1');
disp(index1)
disp('resolution 1 ');
disp(trace.resolution);

% 2/ classification to propose initial fields corresponding to slices of tissue in
% the image
nbpix=20000;    % included pixel for the k-means clustering (note: followed by a manual supervision a posteriori)
imk=kmoyIm(im,5,nbpix); % cluster map, clusters ordered by intensity
% 3/ Supervision du cadrage
[rect,hcad,index]=Hist_Import_cadrage(imk,'manuel',mode,valeur,trace);
ip=strfind(fichiers,'.');  % file name
nom2=fichiers(1:ip(end));  % remove file extension

% save a global view of the fields (traceability)
saveas(hcad,[reps.dataTrace,'Cadrage_',nom2,'.jpg'],'jpeg');

% scale settings

res=trace.resolution(index);
rsc=trace.info(index).Width/trace.info(index1).Width;  
idx=find(trace.resolution==res);
ndx=numel(idx);

    
for i=1:ndx
    imTot(:,:,i)=Hist_Impor_lireImTiff(nomTot,'numero',idx(i),rect.box); 
end
[nx, ny, nc]=size(imTot); 
if nc==1
    phot=Tiff.Photometric.MinIsBlack;
elseif nc==2
    imTot(:,:,3)=imTot(:,:,2);
    phot=Tiff.Photometric.RGB;
    nc=3;
elseif nc==3
    phot=Tiff.Photometric.RGB;
else
    phot=Tiff.Photometric.CFA;
end
   
for i=1:numel(rect)
    box=rect(i).box; box{1}=rsc*box{1};box{2}=rsc*box{2};  % rescale the box in the target scale  
    yy=box{1};xx=box{2};
    yy=max(yy,[1 1]);
    yy=min(yy,[ny ny]);
    xx=max(xx,[1 1]);
    xx=min(xx,[nx nx]);
    imLoc=imTot(xx(1):xx(2),yy(1):yy(2),:);

    [nxL, nyL, nc]=size(imLoc);
    nomSave=[reps.dataImporte,nom2,'_',rect(i).nom,'.tif'];
    t = Tiff(nomSave,'w');  
    tagstruct.ImageLength = nxL; 
    tagstruct.ImageWidth = nyL;
    tagstruct.Photometric = phot ;
    tagstruct.BitsPerSample = 8;
    tagstruct.SamplesPerPixel = nc;
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
    tagstruct.Software = 'MATLAB'; 
    setTag(t,tagstruct)
    write(t,imLoc);
    close(t);

    disp(nomSave)
end
