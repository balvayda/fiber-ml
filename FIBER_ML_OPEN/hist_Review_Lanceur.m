function hist_Review_Lanceur(reps)
% check all : data+roi+results+quality
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.
% reps : structure with directories of reference
close all
disp('>> SUMMARY ...')
info.nomProg=mfilename;
fichp=dir([mfilename,'.m']);


% 0 / directory and name convention checking : list of data/roi/result/qualityControl

[~,fichData]=dirq(reps.ProdData,'');
[~,fichRoi]=dirq(reps.ProdRoi,'');
[~,fichRes]=dirq(reps.ProdRes,'');
[~,fichQual]=dirq(reps.QualProd,'');

nomData=regexprep(fichData,'.tif','');
nomRoi=regexprep(fichRoi,{'roi_','.mat'},{'',''});
nomRes=regexprep(fichRes,{'res_','.mat'},{'',''});
nomQual=regexprep(fichQual,{'Qual_','.mat'},{'',''});

% 1 / id images which exist at different stages of the process Roi, Res et Qual
[~,idDataRoi,idRoi]=intersect(nomData,nomRoi); % index des data ayant une roi
[~,idDataRes,idRes]=intersect(nomData,nomRes);% index data avec résultat, index dans resultat
[~,idDataQual,idQual]=intersect(nomData,nomQual);


% first, only a check, each stage performed or not

nfdat=numel(fichData); 
check=nan(nfdat,1);

isRoi=check;
isRoi(idDataRoi)=1;

isRes=check;
isRes(idDataRes)=1;

isQual=check;
isQual(idDataQual)=1;

prog.data=nomData;
prog.isRoi=isRoi;
prog.isRes=isRes;
prog.isQual=isQual;



progression=struct2table(prog); % summary in a table


% 2/ second table including the counting of each class per image

cmp=0;  % global counter
tab=struct;
for idat=1:nfdat  % for each data/image
    % identification of associated files
    key=nomData{idat};
    if isRoi(idat)>0  % If we have a region of interest for this picture
       fich=[reps.ProdRoi,creeFichier(key,'roi')];
       roi=load(fich);  % result collection
       dateRoi=(dir(fich).date);
       dateNumRoi=(dir(fich).datenum);
    else
        roi=[];
        dateRoi=[];
       dateNumRoi=[];
    end
    
    if isRes(idat)>0  % If we have a result for this picture
       fich=[reps.ProdRes,creeFichier(key,'res')];
       prod=load(fich,'summ');  % result collection
       dateRes=(dir(fich).date);
       dateNumRes=(dir(fich).datenum);
    else
        prod=[];
        dateRes=[];
       dateNumRes=[];
    end

    if isQual(idat)>0  % If we have a quality control for this picture
       fich=[reps.QualProd,creeFichier(key,'qual')];
       qual=load(fich);  % result collection
       dateQual=(dir(fich).date);
       dateNumQual=(dir(fich).datenum);
    else
        qual=[];
        dateQual=[];
       dateNumQual=[];
    end  
    
    if idat==1  % recovery of the name of the classes on the first pass
        if iscell(fichRes)
            fichRes=fichRes{1};
        end
        fich1res=[reps.ProdRes,fichRes];
        prod1=load(fich1res,'summ');        % summary of the results
        nomsClass=prod1.summ(1).compt.nom;  % names of the classes
        nc=numel(nomsClass);
    end
    
    % consistency check between the Regions of Interest of the different stages
    nomsRoiRoi=getNomRoiRoi(roi);
    nomsRoiRes=getNomRoiRes(prod);
    nomsRoiQual=getNomRoiQual(qual);
    checkRoi=1; 
    if compare(nomsRoiRoi,nomsRoiRes)
        if compare(nomsRoiRoi,nomsRoiQual)
            if compare(nomsRoiRes,nomsRoiQual)
                checkRoi=[];  % unless they are well equal (and well-ordered)
            end
        end
    end
                
    if isRoi(idat)>0 ||isRes(idat)>0 || isQual(idat)>0
        %The smallest number of ROIs to fill the table without error. 
        %(If there is a discrepancy, see checkRoi)
        if isQual(idat)>0
            nr=min([numel(nomsRoiRes) numel(nomsRoiQual)]);
        else
            nr=numel(nomsRoiRes);
        end
        if nr==0
            disp('d');
        end
        for iroi=1:nr
            cmp=cmp+1;
            
            % image data            
            tab(cmp).Image=nomData{idat};
            tab(cmp).NumImage=idat;
            
            if numel(prod)>0
                
                tab(cmp).Roi=nomsRoiRes{iroi};       
                nbPix=prod.summ(iroi).compt;%
                for ic=1:nc
                    NameC = nomsClass{ic};
                    NameC = regexprep(NameC,'\W*','_'); % to be compatible with field names
                    tab(cmp).(NameC)=nbPix.brut(ic);
                end
            else
                tab(cmp).Roi=[];
                for ic=nc
                    NameC = nomsClass{ic};
                    NameC = regexprep(NameC,'\W*','_'); % to be compatible with field names
                    tab(cmp).(NameC)=[];
                end
            end
            
            if numel(qual)>0
             
                com=qual.comment;
                tab(cmp).Qvalid=com(iroi).valid;
                tab(cmp).Qimage=com(iroi).image;
                tab(cmp).Qclassif=com(iroi).classif;
                tab(cmp).Qcomment=com(iroi).autre;
            else
                tab(cmp).Qvalid="";
                tab(cmp).Qimage="";
                tab(cmp).Qclassif="";
                tab(cmp).Qcomment="";
            end
            
            % more consistence criteria
            % the risk is : stage n-1 newer than stage n => not uptaded
            % resuts
            % dates
            if ~issorted([dateNumRoi,dateNumRes,dateNumQual])
                tab(cmp).warningDates= 1;
            else
                tab(cmp).warningDates= [];
            end
            tab(cmp).dateRoi=dateRoi;
            tab(cmp).dateRes=dateRes;
            tab(cmp).dateQual=dateQual;
            
            % rois
            
            tab(cmp).warningRois=checkRoi;
            try
                tab(cmp).RoiFromRoi=nomsRoiRoi{iroi};
            catch
                tab(cmp).RoiFromRoi=[];
            end
            try
                tab(cmp).RoiFromRes=nomsRoiRes{iroi};
            catch
                tab(cmp).RoiFromRes=[];
            end
            try
                tab(cmp).RoiFromQual=nomsRoiQual{iroi};
            catch
                tab(cmp).RoiFromQual=[];
            end
        end    
    end
end
% 2-1 / Counting results [RESULTS]
if numel(tab)==1
    synthese=struct2table(tab,'AsArray',true);
else
    synthese=struct2table(tab);
end

dt = datestr(now,'yyyy_mm_dd HH/MM-SS');
dt = regexprep(dt,{'/','-'},{'h','min'});

% save
repRes=reps.ProdRes;  % by default in the root folder
z=split(repRes(1:end-1),filesep); % not very clean
z=join(z(1:end-1),filesep);
if iscell(z)
    z=z{1};
end
repCompil=[z,filesep,'Summary',filesep];
if ~exist(repCompil,'dir')
    mkdir(repCompil);
end


try % if isavailable excel
    nomTab=[repCompil,'CheckList',dt,'.xlsx'];
    writetable(progression,nomTab,'WriteRowNames',true);
    nomTab=[repCompil,'Sythesis',dt,'.xlsx'];
    writetable(synthese,nomTab,'WriteRowNames',true);
    
catch
    nomTab=[repCompil,'Checklist',dt,'.csv'];
    writetable(progression,nomTab,'WriteRowNames',true,'delimiter','tab');
    nomTab=[repCompil,'Sythesis',dt,'.csv'];
    writetable(synthese,nomTab,'WriteRowNames',true,'delimiter','tab');

end

msgbox(nomTab,'Summary saved in:')
disp('Summary saved in:');
disp(nomTab); 
disp('*******');
disp('<< SUMMARY');

end

function noms=getNomRoiRes(prod)
noms={};
if ~isempty(prod)
    res=prod.summ;
    nr=numel(res);
    noms=cell(nr,1);
    for i=1:nr
        if isfield(res(i),'roi') && ~isempty(res(i).roi)
            noms{i}=res(i).roi;
        else
            noms{i}='ROI 1';
        end
    end
end
end

function noms=getNomRoiQual(qual)
noms={};
if ~isempty(qual) && isfield(qual,'nomRois')
    noms=qual.nomRois;
else
    noms='ROI 1';
end
end

function noms=getNomRoiRoi(roi)
noms={};
if ~isempty(roi)
    noms={roi.pgs.DisplayName}';
end
end

function nom= creeFichier(cle,type)

% full file names associated with a key
switch type
    case 'data'
        nom=[cle,'.tif'];
    case 'roi'
        nom=['roi_',cle,'.mat'];
    case 'res'
        nom=['res_',cle,'.mat'];
    case 'qual'
        nom=['Qual_',cle,'.mat'];  % uppercase to be reviewed for future versions
end

end

function out=compare(a,b)
% note: not symetric
if isequal(a,b) || isempty (b)
    out =1 ;
else
    out =0;
end
end