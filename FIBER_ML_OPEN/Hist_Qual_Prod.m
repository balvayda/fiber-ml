function Hist_Qual_Prod(runon,nbTau,reps)
% % for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

% launch the interface to evaluate the quality of the process
% IN
% runon : (run on) type of results to process ('treated','untreated'...)
% nbTau : size of the sample data to test (number of image or ration of the
% stack
% reps : structure with the name of every directory involved in the Fiber
% process

% 1/ identification of available study data and results
disp('>> Quality Control');

[~,fichData]=dirq(reps.ProdData,'');
[~,fichRes]=dirq(reps.ProdRes,'');


 % selection of pairs (data-result) = (data-classification)
nomData=regexprep(fichData,'.tif','');
nomRes=regexprep(fichRes,{'res_','.mat'},{'',''});
[~,iData,iRes]=intersect(nomData,nomRes);   % indices
nd= numel(iData);

%%%%%%%%%%%%%data selection
switch runon % 
    case 'manual'  % manual option
        ind=listdlg('ListString',nomData(iData));  
        nbTau=numel(ind);
    otherwise
                    % random sampling option
                    
        if nbTau>1 % number of results to select..
            nbTau=round(min(nbTau,nd));
        else        % ..or fraction of results to select
            nbTau=round(nbTau*nd);
        end
        % ..for the quality control
        ind=randperm(nd,nbTau);
        
end
iData=iData(ind);
iRes=iRes(ind);
%%%%%%%%%%%%%


    
nd= numel(iData); %size of the sample
cmp=0;
toStop=0;
for i=1:nd % evaluation image per image / or ROI per ROI
    % catch a pair
    
    if ~toStop
        nomFichData=fichData{iData(i)};
        nomFichRes=fichRes{iRes(i)};
        
        nomD=nomData{iData(i)};
        nomR=nomRes{iRes(i)};
        
        if ~isequal(nomR,nomD)
            error('file mismatch between study data and classification of the prod phase');
        end
        
        im=imread([reps.ProdData,nomFichData]);       % reading of the ith image
        pro=load([reps.ProdRes,nomFichRes]);
        
        nr=numel(pro.res);
        
        for j=1:nr
            if toStop==0
                % display
                
                cmp=cmp+1;  % counter
                
                hf=figure('units','normalized','position',[0.01 0.01 .7 0.9]) ;
                set(hf,'MenuBar','none','ToolBar','none');
                set(hf,'DeleteFcn',{@fermeture,hf});
                him=imagesc(im); axis image;
                ha=gca;
                ha.Toolbar.Visible='on';
                setappdata(him,'im',im);
                
                
                insert.trans=1; % map transparency/visibility < > transparence
                insert.map=1;   % map type v ^
                insert.com=2;  %  add a form
                insert.close=1; % can close
                
                % display  and interaction
                roi=pro.res(j).classif.roi;
                vari=pro.info.listApp.variable;
                classif=pro.res(j).classif;
                
                if isfield(roi,'nomRoi')
                    nameRoi=roi.nomRoi;
                else
                    nameRoi='ROI 1';
                end
                
                title([nomD,'_',nameRoi],'Interpreter','none');
                
                %%%%%%%%%%%%%% provides annotations from hf : setappdata(hf)
                Hist_Qual_Core(im,him,insert,roi,vari,classif);
                %%%%%%%%%%%%%%
                waitfor(hf,'Tag');
                if isequal(hf.Tag,'stop')  % si stop was clicked, do nothing
                    disp('interruption scan qualite operateur');
                    toStop=1;
                    close(hf);
                    break
                else
                    toStop=0;
                    if j==1
                        comment=getappdata(hf,'comment');
                    else
                        comment(j)=getappdata(hf,'comment');
                    end
                    Comment(cmp)=comment(j);  % compilation of the comments
                    
                    nomQ=['Qual_',nomD,'.mat'];
                    
                    if j==1
                        clear info
                    end
                    info(j).nomData=nomD;
                    info(j).i=i;
                    info(j).nomRoi=nameRoi;
                    Info(cmp)=info(j); % compilation of additionnal information
                    
                    if j==1
                        clear nomRois
                    end
                    nomRois{j,1}=nameRoi;
                    
                    close(hf);
                end
            end
        end
        if ~toStop
            save([reps.QualProd,nomQ],'comment','info','nomRois');
        end
        clear comment mr nomRois % variables only for 1 image
    else
        break
    end
end

tab=cat(2,struct2table(Info),struct2table(Comment));
dt = datestr(now,'yyyy_mm_dd HH/MM-SS');
dt = regexprep(dt,{'/','-'},{'h','min'});
try % if isavailable excel
    nameTab=[reps.QualProdTab,'QualProd',dt,'.xlsx'];
    writetable(tab,nameTab);
catch
    nameTab=[reps.QualProdTab,'QualProd',dt,'.csv'];
    writetable(tab,nameTab,'delimiter','tab');
end

disp(['Table, see: ',nameTab]); 
disp('<< Quality Control');


function fermeture(~,~,hf) % close is clicked

hn=getappdata(hf,'hnotif');
if ~isempty(hn)
    close(hn);  % fermer la notification associ�e si celle-ci existe
end
%disp(getappdata(hf,'comment'));


