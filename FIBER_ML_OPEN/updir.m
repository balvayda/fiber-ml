function repout=updir(repin)
% parent directory
tags=split(repin,filesep);
res=join(tags(1:end-1),filesep);
repout=res{1};