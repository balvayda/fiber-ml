function [imk,coul]=kmoyIm(im,k,npix,option)
% k-means clustering sampling for images (including ordering and sampling)

% im : image
% k : number of classes
% npix :sample size to calculate class representatives from each class
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.

[nx,ny,nz]=size(im);
np=nx*ny;                       % number of pixels in the image
nr=min([np npix]);   % number of pixel to consider to calculate centroids of classes

% sampling

im=reshape(im,[np nz]); % list of pixels
itest = randperm(np,nr); % random selection of indices
imLt=im(itest,:);       % sample of pixels


% kmeans processing to evaluate centroids
if exist('option','var')
    [~,coul]=kmeans(double(imLt),k,'emptyaction','singleton','start','uniform','replicate',6,'option',option);%,'distance','cityblock'
else
    [~,coul]=kmeans(double(imLt),k,'emptyaction','singleton','start','uniform','replicate',6);%,'distance','cityblock'
end
% sort clusters by intensity
lum=mean(coul,2);
[~,ilum]=sort(lum);
coul=coul(ilum,:);

% assign to each pixel of the image a class corresponding to the nearest centroid
D = pdist2(reshape(im,[np 3]),coul);
[~,iq]=min(D,[],2);

imk=reshape(iq,[nx ny]);
end


