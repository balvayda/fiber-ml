function table=Hist_Prod_Lanceur(runon,runroi,reps)
% provides classification of the study data from a training list
% IN
% runon : tag: type of data to process
% runroi : tag : on ROIs only ?
% reps : directory names
% for FIBER-LM1.0 D.Balvay 07 january 2022, PARCC U970 Inserm/Univ. Paris, France.
close all
disp('>> PRODUCTION ...')
info.nomProg=mfilename;
info.nomProg=[info.nomProg,'.m'];

fichp=dir(info.nomProg);
 % management of the compiled version
if isempty(fichp)
    fichp=dir('FIBER.exe');
end
if ~isempty(fichp)
    info.dateProg=fichp.date;
end

% 0 / preparatory stage : 

[~,fichData]=dirq(reps.ProdData,'');
[~,fichRoi]=dirq(reps.ProdRoi,'');
[~,fichRes]=dirq(reps.ProdRes,'');

nomData=regexprep(fichData,'.tif','');
nomRoi=regexprep(fichRoi,{'roi_','.mat'},{'',''});
nomRes=regexprep(fichRes,{'res_','.mat'},{'',''});

s = settings;           % accept parametric maps (to write) beyond 2Go
s.matlab.general.matfile.SaveFormat.TemporaryValue = 'v7.3';

% 0-1 Images to be processed
% analyse files in data/Roi/results; link all with data

switch runon % data to process
    case 'untreated'
        % Data which were not previously processed (no result detected)
        [~,iData]=setxor(nomData,nomRes);
        iDataF=iData';
    case 'treated'
        % redo the results already produced (but we suppose with a correction)
        [~,iData,~]=intersect(nomData,nomRes);
        iDataF=iData';
    case 'all'
        % Relaunch everything
        iDataF=(1:numel(nomData));
    case 'manual'
        [iDataF,~]=listdlg('ListString',nomData);  
end
if runroi   
    % reduce to images with ROI
    [~,iData]=intersect(nomData,nomRoi);
    iDataR=iData';
else
    % no reduction, process with and without ROI
    iDataR=(1:numel(nomData));
end

% combine both criteria of selection
iData=intersect(iDataR,iDataF);      % data to process
nd=numel(iData);  % number of data (images)

% 0-2 Select a learning list (collectePicClass...)

[fichList,repList]=uigetfile(reps.AppList);
liste=load([repList,fichList],'rangement'); % read it
        % liste.pixel(couleurs) .classes(cat�gories)
        % .classeDef(d�finition,nom et couleur)...
if isempty(liste) || ~isfield(liste,'rangement')
    error('Empty learning list - in production');
end
    
nc=numel(liste.rangement.classeDef.nom); % number of classes
        

col1=cell(nd,3);        % taille minimales des noms et valeurs
table1=cell(nd,nc);     % nb data x nb classes
j=0;          % counter
disp([num2str(nd),' image/s to perform']);
cmp=0;

if nd==0
    msgbox('No image to process, see: Processing on..');
    return
end
for i=1:nd
    %try
        nomFichData=fichData{iData(i)};
        im=imread([reps.ProdData,nomFichData]);                   % active ith image 
        nomDataI=nomData{iData(i)};
        nomDataI=regexprep(nomDataI,'.TIF','.tif');

        disp(nomDataI);                                     % follow the processing for debugging

        [~,~,ir]=intersect(nomDataI,nomRoi); % id of the Roi file to read (one file for several ROIS)
        hroi=[];
        if ~isempty(fichRoi) && ~isempty(ir) && exist([reps.ProdRoi,fichRoi{ir}],'file')
            hroi=load([reps.ProdRoi,fichRoi{ir}]);  % lead roi : polyshape / polygon
        end 
         % one or several ROIS
        if isfield(hroi,'pgs')
            hroi=hroi.pgs;
        end
        if ~isempty (hroi)
            nhr=numel(hroi);    % number of drawn ROIS
            isaroi=1;
        else
            nhr=1;  % all the image
            isaroi=0;
        end
        disp(['-- ', num2str(nhr),' ROI/s to perform']);
        res=struct();
        summ=struct();
        
        for j=1:nhr
            cmp=cmp+1; % new line of result (one per ROI) in the output table
            if isaroi
                h1roi=hroi(j);  % open ROI j
                if isfield(h1roi,'masque') % Temporary, compatible with older version
                    h1roi.masc=h1roi.masque;
                    h1roi=rmfield(h1roi,'masque');
                    h1roi.nomRoi='ROI 1';  % new field vs. old version
                end
                if ~isfield(h1roi,'masc')
                    pg=h1roi;  % older again, for older data
                    h1roi=[]; % useful ?
                    h1roi.pg = pg.Shape;
                    h1roi.masc = pg.UserData.masc;
                    h1roi.nomRoi = pg.DisplayName;
                end
            else
                h1roi=[];
            end
            col1{cmp,1}=regexprep(nomDataI,'.tif',''); % traceability
            col1{cmp,2}=i;
            if ~isempty(h1roi) && isfield(h1roi,'nomRoi')
                nameOfRoi=h1roi.nomRoi;
            else
                nameOfRoi='ROI 1';
            end
            col1{cmp,3}=nameOfRoi;
            rangement=liste.rangement;
            % classification
            if j==1 % first use, the classification model is fitted from the learning list...
                [classif,cls]=Hist_Class_Core(im,h1roi,rangement); 
            else                                %...then useless fitting, we get the model back fitted
                classif=Hist_Class_Core(im,h1roi,rangement,cls); 
            end

            % display classification map
            hf=figure;

            nom=[nomDataI,'_',nameOfRoi];
            him=imagesc(im);axis image;title (nom,'Interpreter','latex');
            setappdata(him,'im',im);  % WARNING : justify 


            interface=[];  % not interactive figures
            Hist_Qual_Core(im,him,interface,h1roi,classif.listApp,classif,cls);
            nom=regexprep(nom,'\W','_');
            nomFig=[reps.ProdResFig,'fig_Brut_',nom];
            saveas(hf,[nomFig,'.fig']);
            saveas(hf,[nomFig,'.tif'],'tif');

            % res : results synthesis

            res(j).classif=classif;  % a result per ROI
            summ(j).compt=classif.compt; % shortcut for quick access by summary
            summ(j).roi=nameOfRoi;
            info.modele=cls; % warning rewriting
            info.listeApp.fichier=fichList;
            info.listApp.repertoire=repList;
            info.listApp.variable=rangement;
            info.data=nomFichData;

            for k=1:nc   % write a new line in the output table
                table1{cmp,k}=res(j).classif.compt.brut(k);
            end
            
            close(hf);
        end % per ROI
        
        % save the corresponding mat file
        
        nomRes=['res_',nomDataI,'.mat'];
        save([reps.ProdRes,nomRes],'res','info','summ','-v7.3');
        
%     catch ME   % if an exception (error) occurs for an image, continue with other ones
%         warning(['An Error occured for: ',nomDataI]);
%         disp(ME.message);
%     end
end

% tab of synthesis
entete=res(1).classif.compt.nom;
col1=cat(1,{'ID Data','N� Data','ROI Name'},col1);
table1e=cat(1,entete,table1); % include header
table1e=cat(2,col1,table1e);

dt = datestr(now,'yyyy_mm_dd HH/MM-SS');
dt = regexprep(dt,{'/','-'},{'h','min'});

nomTable=[reps.ProdResTab,'Counting_',dt];
[FileName,PathName] = uiputfile('*.xlsx','SauvegardeComptabilite',nomTable);

if FileName %  "cancel" not clicked
    try
        disp('attempt xlswrite')
        xlswrite([PathName,FileName],table1e,'brute');

    catch
        try
            disp('attempt writeTableMultiFeuilles')
            col1=col1(2:end);
            tab1=cell2table(table1,'VariableNames',entete,'RowNames',col1);
            nom=regexprep(FileName,'\.','_tab\.');
            writetable(tab1,[PathName,nom],'Sheet','brute','WriteRowNames',true);
        catch

            try
                disp('attempt 3writeTable1Feuilles')
                nom=regexprep(FileName,'\.','_brute\.');
                writetable(tab1,[PathName,nom]);
            catch
                disp('tableMatlab')
                nom=regexprep(FileName,'.xlsx','.mat');
                save([PathName,nom],'table1','info','-v7.3');
            end
        end
    end
    disp('Saved :');
    disp([PathName,FileName]);
else
    disp('trace not saved');
end
disp('<< PRODUCTION ...')